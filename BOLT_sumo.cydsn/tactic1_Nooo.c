/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "tactic1_Nooo.h"
#include "sensors.h"
#include "timer_time.h"
#include "drive.h"

//=============================================================================
// Private defines and typedefs
//=============================================================================
#define Rd_bits(value, mask)        ((value)&(mask))

#define SEARCH_PWM_L  25
#define SEARCH_PWM_R  25
#define TURN_PWM    40
#define ATTACK_PWM  80

//=============================================================================
// Private function declarations
//=============================================================================
static void ActOnEnemySpotted(void);
static void ActOnLineDetected(void);
static void DoSomething(void);

//=============================================================================
// Private data definitions
//=============================================================================
static uint8_t lineSens = 0;
static uint8_t distSens = 0;
static uint8_t side = 0;


//=============================================================================
// Public function definitions
//=============================================================================

//-----------------------------------------------------------------------------
//! \brief	Combat tactic #1 in action
//-----------------------------------------------------------------------------
void RunTactic1()
{
    WaitForIRStart();
    
    // ============================ First move ================================

    drive_forward(gMotor.basic_speed, gMotor.basic_speed);
    CyDelay(50);
    //ResetDistSensors();

    // ============================= Main loop ================================
    uint32_t timout_time = millis();
    while (1) {
		// read all sensors
		lineSens = LineSensRead();
	    distSens =  DistSensRead();
        //ResetDistSensors();
        

		if( 0 < lineSens ){
			// line has detected
		    ActOnLineDetected();
			timout_time = millis();
		}
		else if( 0 < distSens ){
			// opponent spotted
			ActOnEnemySpotted();
			timout_time = millis();
		}
        else {
            drive_forward(0, 0);
            //CyDelay(50);
        }

		if( 1000 < millis() - timout_time ){
			timout_time = millis();
			DoSomething();
            Servo_enable_Write(0);
		}
    }
}

//=============================================================================
// Private function definitions
//=============================================================================

//! \brief	Act on opponent spotted event
void ActOnEnemySpotted(void){


	    int distSens = DistSensRead(); 
        //Distance_sensor_value_Read();
        //ResetDistSensors();
        
    	if( 0 < distSens ){
            int8_t avrg_angle_grad = CalcAvrgAngle();

        	int8_t pwm = 60;
            // Attack spotted opponent
        	if( 0 == avrg_angle_grad ){
        		drive_forward( ATTACK_PWM , ATTACK_PWM );		// drive forward
        		CyDelay(40);
        	}else if( -60 >= avrg_angle_grad ){
        		// [-90; -60] grad
        		turn_left(TURN_PWM,TURN_PWM); //drive_motors( -pwm, pwm );		// turn on spot left
                CyDelay(40);
        	}else if( -30 >= avrg_angle_grad ){
        		// (-60; -30] grad
        		drive_forward( pwm / 3, pwm );	// drive more left
                CyDelay(30);
        	}else if( 0 > avrg_angle_grad ){
        		// (-30; 0) grad
        		drive_forward( ATTACK_PWM * 0.7, ATTACK_PWM );	// drive little left
                CyDelay(40);
        	}else if( 60 <= avrg_angle_grad ){
        		// [60; 90] grad
        		turn_right(TURN_PWM,TURN_PWM); //drive_motors( pwm, -pwm );		// turn on spot right
                CyDelay(40);
        	}else if( 30 <= avrg_angle_grad ){
        		// [30; 60) grad
        		drive_forward( pwm, pwm / 3 );	// drive more right
                CyDelay(30);
        	}else if( 0 <= avrg_angle_grad ){
        		// (0; 30) grad
        		drive_forward( ATTACK_PWM, ATTACK_PWM * 0.7 );	// drive little right
                CyDelay(40);
        	}
            CyDelay(10);

    		}
        else {
            drive_forward(0, 0);
            //CyDelay(50);
        }
       
}

//! \brief	Act on line detected event
void ActOnLineDetected(void){
	//int lineSens = LineSensRead();
     
	// Line_FL && Line_FR
	if(gLine.F_LEFT && gLine.F_RIGHT){
        drive_backward(40,40); //drive_distance_straight_backward(10, 40);
        CyDelay(400);
        turn_left(TURN_PWM,TURN_PWM); //turn_angle_right(180, TURN_PWM );
        CyDelay(300);
	}
	// Line_FL
	else if(gLine.F_LEFT){
        drive_backward(40,40); //drive_distance_straight_backward(10, 40);
        CyDelay(150);
        turn_right(TURN_PWM,TURN_PWM); //turn_angle_right(135, TURN_PWM );
        CyDelay(200);
	}
	// Line_FR
	else if(gLine.F_RIGHT){
        drive_backward(40,40); //drive_distance_straight_backward(10, 40);
        CyDelay(150);
        turn_left(TURN_PWM,TURN_PWM); //turn_angle_left(135, TURN_PWM );
        CyDelay(200);
	}
    //drive_forward( 0, 0 );
}

//! \brief	Do something when opponent is not visible
void DoSomething(void){
	// Drive little forward.
    if (side <= 0) {
        side = 1;
        turn_left(TURN_PWM,TURN_PWM);
        CyDelay(30);
    }
    else {
        side = 0;
        turn_right(TURN_PWM,TURN_PWM);
        CyDelay(30);
    }
    drive_forward( 0, 0 ); // BREAK A BIT FOR FUN
    CyDelay(10);
}
/* [] END OF FILE */
