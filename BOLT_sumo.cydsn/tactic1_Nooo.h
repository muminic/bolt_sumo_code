/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef TACTIC_1_H
#define TACTIC_1_H

//=============================================================================
// Public function declarations
//=============================================================================
void RunTactic1(void);

#endif /* TACTIC1_H */
/* [] END OF FILE */