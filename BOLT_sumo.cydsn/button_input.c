/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "button_input.h"
#include "stdio.h"

#define KEYCODE_ENTER_MASK			0x01
#define KEYCODE_UP_MASK				0x02
#define KEYCODE_DOWN_MASK			0x04

#define Rd_bits( value, mask)        ((value)&(mask))
#define Clr_bits(lvalue, mask)  ((lvalue) &= ~(mask))
#define Set_bits(lvalue, mask)  ((lvalue) |=  (mask))

buttoncode_t IsKeyReleased(void){
	keyboard_event_t input;
	buttoncode_t buttoncode = KEYCODE_NO_KEY;

	GetButtonState(&input);
	if( KEYACTION_RELEASE == input.buttonaction ){
		buttoncode = input.buttoncode;
	}

	return buttoncode;
}

int GoBack()
{
    keyboard_event_t input;
	buttoncode_t goBackState = 0;

	GetButtonState(&input);
	if( KEYACTION_RELEASE == input.buttonaction ){
        if (input.buttoncode == KEYCODE_ENTER)
        {
            
		    goBackState = 1;
        }
	}

	return goBackState;
    
}


int ReadUpDownState()
{
    keyboard_event_t input;
	buttoncode_t UpDownState = 0;

	GetButtonState(&input);
	if( KEYACTION_RELEASE == input.buttonaction ){
        if (input.buttoncode == KEYCODE_UP)
        {
		    UpDownState = 1;
        }
        if (input.buttoncode == KEYCODE_DOWN)
        {
		    UpDownState = 2;
        }
	}


	return UpDownState;
}

void GetButtonState(keyboard_event_t *keyboardEvent){
	// Variable to hold the key mask
	static uint8_t keyboardState;

	if( 0 == poga_2_Read() && !Rd_bits(keyboardState, KEYCODE_ENTER_MASK) ){
		// ENTER key pressed
		Set_bits(keyboardState, KEYCODE_ENTER_MASK);
		keyboardEvent->buttoncode = KEYCODE_ENTER;
		keyboardEvent->buttonaction = KEYACTION_PRESS;

	}else if( 1 == poga_2_Read() && Rd_bits(keyboardState, KEYCODE_ENTER_MASK) ){
		// ENTER key released
		Clr_bits(keyboardState, KEYCODE_ENTER_MASK);
		keyboardEvent->buttoncode = KEYCODE_ENTER;
		keyboardEvent->buttonaction = KEYACTION_RELEASE;

	}else if( 0 == poga_1_Read() && !Rd_bits(keyboardState, KEYCODE_UP_MASK) ){
		// UP key pressed
		Set_bits(keyboardState, KEYCODE_UP_MASK);
		keyboardEvent->buttoncode = KEYCODE_UP;
		keyboardEvent->buttonaction = KEYACTION_PRESS;

	}else if( 1 == poga_1_Read() && Rd_bits(keyboardState, KEYCODE_UP_MASK) ){
		// UP key released
		Clr_bits(keyboardState, KEYCODE_UP_MASK);
		keyboardEvent->buttoncode = KEYCODE_UP;
		keyboardEvent->buttonaction = KEYACTION_RELEASE;

	}else if( 0 == poga_3_Read() && !Rd_bits(keyboardState, KEYCODE_DOWN_MASK) ){
		// DOWN key pressed
		Set_bits(keyboardState, KEYCODE_DOWN_MASK);
		keyboardEvent->buttoncode = KEYCODE_DOWN;
		keyboardEvent->buttonaction = KEYACTION_PRESS;

	}else if( 1 == poga_3_Read() && Rd_bits(keyboardState, KEYCODE_DOWN_MASK) ){
		// DOWN key released
		Clr_bits(keyboardState, KEYCODE_DOWN_MASK);
		keyboardEvent->buttoncode = KEYCODE_DOWN;
		keyboardEvent->buttonaction = KEYACTION_RELEASE;

	}else{
		// No keyboard key actions detected
		keyboardEvent->buttoncode = KEYCODE_NO_KEY;
		keyboardEvent->buttonaction = KEYACTION_NO_ACTION;
	}
}
/* [] END OF FILE */
