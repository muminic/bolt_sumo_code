/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef TACTIC_0_H
#define TACTIC_0_H

//=============================================================================
// Public function declarations
//=============================================================================
void RunTactic0(int firstMove,int speedlimit);

#endif /* TACTIC1_H */
/* [] END OF FILE */
