/*******************************************************************************
* File Name: ENC_L2.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ENC_L2_H) /* Pins ENC_L2_H */
#define CY_PINS_ENC_L2_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "ENC_L2_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 ENC_L2__PORT == 15 && ((ENC_L2__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    ENC_L2_Write(uint8 value);
void    ENC_L2_SetDriveMode(uint8 mode);
uint8   ENC_L2_ReadDataReg(void);
uint8   ENC_L2_Read(void);
void    ENC_L2_SetInterruptMode(uint16 position, uint16 mode);
uint8   ENC_L2_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the ENC_L2_SetDriveMode() function.
     *  @{
     */
        #define ENC_L2_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define ENC_L2_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define ENC_L2_DM_RES_UP          PIN_DM_RES_UP
        #define ENC_L2_DM_RES_DWN         PIN_DM_RES_DWN
        #define ENC_L2_DM_OD_LO           PIN_DM_OD_LO
        #define ENC_L2_DM_OD_HI           PIN_DM_OD_HI
        #define ENC_L2_DM_STRONG          PIN_DM_STRONG
        #define ENC_L2_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define ENC_L2_MASK               ENC_L2__MASK
#define ENC_L2_SHIFT              ENC_L2__SHIFT
#define ENC_L2_WIDTH              1u

/* Interrupt constants */
#if defined(ENC_L2__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ENC_L2_SetInterruptMode() function.
     *  @{
     */
        #define ENC_L2_INTR_NONE      (uint16)(0x0000u)
        #define ENC_L2_INTR_RISING    (uint16)(0x0001u)
        #define ENC_L2_INTR_FALLING   (uint16)(0x0002u)
        #define ENC_L2_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define ENC_L2_INTR_MASK      (0x01u) 
#endif /* (ENC_L2__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define ENC_L2_PS                     (* (reg8 *) ENC_L2__PS)
/* Data Register */
#define ENC_L2_DR                     (* (reg8 *) ENC_L2__DR)
/* Port Number */
#define ENC_L2_PRT_NUM                (* (reg8 *) ENC_L2__PRT) 
/* Connect to Analog Globals */                                                  
#define ENC_L2_AG                     (* (reg8 *) ENC_L2__AG)                       
/* Analog MUX bux enable */
#define ENC_L2_AMUX                   (* (reg8 *) ENC_L2__AMUX) 
/* Bidirectional Enable */                                                        
#define ENC_L2_BIE                    (* (reg8 *) ENC_L2__BIE)
/* Bit-mask for Aliased Register Access */
#define ENC_L2_BIT_MASK               (* (reg8 *) ENC_L2__BIT_MASK)
/* Bypass Enable */
#define ENC_L2_BYP                    (* (reg8 *) ENC_L2__BYP)
/* Port wide control signals */                                                   
#define ENC_L2_CTL                    (* (reg8 *) ENC_L2__CTL)
/* Drive Modes */
#define ENC_L2_DM0                    (* (reg8 *) ENC_L2__DM0) 
#define ENC_L2_DM1                    (* (reg8 *) ENC_L2__DM1)
#define ENC_L2_DM2                    (* (reg8 *) ENC_L2__DM2) 
/* Input Buffer Disable Override */
#define ENC_L2_INP_DIS                (* (reg8 *) ENC_L2__INP_DIS)
/* LCD Common or Segment Drive */
#define ENC_L2_LCD_COM_SEG            (* (reg8 *) ENC_L2__LCD_COM_SEG)
/* Enable Segment LCD */
#define ENC_L2_LCD_EN                 (* (reg8 *) ENC_L2__LCD_EN)
/* Slew Rate Control */
#define ENC_L2_SLW                    (* (reg8 *) ENC_L2__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define ENC_L2_PRTDSI__CAPS_SEL       (* (reg8 *) ENC_L2__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define ENC_L2_PRTDSI__DBL_SYNC_IN    (* (reg8 *) ENC_L2__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define ENC_L2_PRTDSI__OE_SEL0        (* (reg8 *) ENC_L2__PRTDSI__OE_SEL0) 
#define ENC_L2_PRTDSI__OE_SEL1        (* (reg8 *) ENC_L2__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define ENC_L2_PRTDSI__OUT_SEL0       (* (reg8 *) ENC_L2__PRTDSI__OUT_SEL0) 
#define ENC_L2_PRTDSI__OUT_SEL1       (* (reg8 *) ENC_L2__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define ENC_L2_PRTDSI__SYNC_OUT       (* (reg8 *) ENC_L2__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(ENC_L2__SIO_CFG)
    #define ENC_L2_SIO_HYST_EN        (* (reg8 *) ENC_L2__SIO_HYST_EN)
    #define ENC_L2_SIO_REG_HIFREQ     (* (reg8 *) ENC_L2__SIO_REG_HIFREQ)
    #define ENC_L2_SIO_CFG            (* (reg8 *) ENC_L2__SIO_CFG)
    #define ENC_L2_SIO_DIFF           (* (reg8 *) ENC_L2__SIO_DIFF)
#endif /* (ENC_L2__SIO_CFG) */

/* Interrupt Registers */
#if defined(ENC_L2__INTSTAT)
    #define ENC_L2_INTSTAT            (* (reg8 *) ENC_L2__INTSTAT)
    #define ENC_L2_SNAP               (* (reg8 *) ENC_L2__SNAP)
    
	#define ENC_L2_0_INTTYPE_REG 		(* (reg8 *) ENC_L2__0__INTTYPE)
#endif /* (ENC_L2__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_ENC_L2_H */


/* [] END OF FILE */
