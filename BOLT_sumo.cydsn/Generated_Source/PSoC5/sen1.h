/*******************************************************************************
* File Name: sen1.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen1_H) /* Pins sen1_H */
#define CY_PINS_sen1_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen1_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen1__PORT == 15 && ((sen1__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen1_Write(uint8 value);
void    sen1_SetDriveMode(uint8 mode);
uint8   sen1_ReadDataReg(void);
uint8   sen1_Read(void);
void    sen1_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen1_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen1_SetDriveMode() function.
     *  @{
     */
        #define sen1_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen1_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen1_DM_RES_UP          PIN_DM_RES_UP
        #define sen1_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen1_DM_OD_LO           PIN_DM_OD_LO
        #define sen1_DM_OD_HI           PIN_DM_OD_HI
        #define sen1_DM_STRONG          PIN_DM_STRONG
        #define sen1_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen1_MASK               sen1__MASK
#define sen1_SHIFT              sen1__SHIFT
#define sen1_WIDTH              1u

/* Interrupt constants */
#if defined(sen1__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen1_SetInterruptMode() function.
     *  @{
     */
        #define sen1_INTR_NONE      (uint16)(0x0000u)
        #define sen1_INTR_RISING    (uint16)(0x0001u)
        #define sen1_INTR_FALLING   (uint16)(0x0002u)
        #define sen1_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen1_INTR_MASK      (0x01u) 
#endif /* (sen1__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen1_PS                     (* (reg8 *) sen1__PS)
/* Data Register */
#define sen1_DR                     (* (reg8 *) sen1__DR)
/* Port Number */
#define sen1_PRT_NUM                (* (reg8 *) sen1__PRT) 
/* Connect to Analog Globals */                                                  
#define sen1_AG                     (* (reg8 *) sen1__AG)                       
/* Analog MUX bux enable */
#define sen1_AMUX                   (* (reg8 *) sen1__AMUX) 
/* Bidirectional Enable */                                                        
#define sen1_BIE                    (* (reg8 *) sen1__BIE)
/* Bit-mask for Aliased Register Access */
#define sen1_BIT_MASK               (* (reg8 *) sen1__BIT_MASK)
/* Bypass Enable */
#define sen1_BYP                    (* (reg8 *) sen1__BYP)
/* Port wide control signals */                                                   
#define sen1_CTL                    (* (reg8 *) sen1__CTL)
/* Drive Modes */
#define sen1_DM0                    (* (reg8 *) sen1__DM0) 
#define sen1_DM1                    (* (reg8 *) sen1__DM1)
#define sen1_DM2                    (* (reg8 *) sen1__DM2) 
/* Input Buffer Disable Override */
#define sen1_INP_DIS                (* (reg8 *) sen1__INP_DIS)
/* LCD Common or Segment Drive */
#define sen1_LCD_COM_SEG            (* (reg8 *) sen1__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen1_LCD_EN                 (* (reg8 *) sen1__LCD_EN)
/* Slew Rate Control */
#define sen1_SLW                    (* (reg8 *) sen1__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen1_PRTDSI__CAPS_SEL       (* (reg8 *) sen1__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen1_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen1__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen1_PRTDSI__OE_SEL0        (* (reg8 *) sen1__PRTDSI__OE_SEL0) 
#define sen1_PRTDSI__OE_SEL1        (* (reg8 *) sen1__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen1_PRTDSI__OUT_SEL0       (* (reg8 *) sen1__PRTDSI__OUT_SEL0) 
#define sen1_PRTDSI__OUT_SEL1       (* (reg8 *) sen1__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen1_PRTDSI__SYNC_OUT       (* (reg8 *) sen1__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen1__SIO_CFG)
    #define sen1_SIO_HYST_EN        (* (reg8 *) sen1__SIO_HYST_EN)
    #define sen1_SIO_REG_HIFREQ     (* (reg8 *) sen1__SIO_REG_HIFREQ)
    #define sen1_SIO_CFG            (* (reg8 *) sen1__SIO_CFG)
    #define sen1_SIO_DIFF           (* (reg8 *) sen1__SIO_DIFF)
#endif /* (sen1__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen1__INTSTAT)
    #define sen1_INTSTAT            (* (reg8 *) sen1__INTSTAT)
    #define sen1_SNAP               (* (reg8 *) sen1__SNAP)
    
	#define sen1_0_INTTYPE_REG 		(* (reg8 *) sen1__0__INTTYPE)
#endif /* (sen1__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen1_H */


/* [] END OF FILE */
