/*******************************************************************************
* File Name: ENC_L_Counter_PM.c  
* Version 3.0
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "ENC_L_Counter.h"

static ENC_L_Counter_backupStruct ENC_L_Counter_backup;


/*******************************************************************************
* Function Name: ENC_L_Counter_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  ENC_L_Counter_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void ENC_L_Counter_SaveConfig(void) 
{
    #if (!ENC_L_Counter_UsingFixedFunction)

        ENC_L_Counter_backup.CounterUdb = ENC_L_Counter_ReadCounter();

        #if(!ENC_L_Counter_ControlRegRemoved)
            ENC_L_Counter_backup.CounterControlRegister = ENC_L_Counter_ReadControlRegister();
        #endif /* (!ENC_L_Counter_ControlRegRemoved) */

    #endif /* (!ENC_L_Counter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: ENC_L_Counter_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  ENC_L_Counter_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void ENC_L_Counter_RestoreConfig(void) 
{      
    #if (!ENC_L_Counter_UsingFixedFunction)

       ENC_L_Counter_WriteCounter(ENC_L_Counter_backup.CounterUdb);

        #if(!ENC_L_Counter_ControlRegRemoved)
            ENC_L_Counter_WriteControlRegister(ENC_L_Counter_backup.CounterControlRegister);
        #endif /* (!ENC_L_Counter_ControlRegRemoved) */

    #endif /* (!ENC_L_Counter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: ENC_L_Counter_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  ENC_L_Counter_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void ENC_L_Counter_Sleep(void) 
{
    #if(!ENC_L_Counter_ControlRegRemoved)
        /* Save Counter's enable state */
        if(ENC_L_Counter_CTRL_ENABLE == (ENC_L_Counter_CONTROL & ENC_L_Counter_CTRL_ENABLE))
        {
            /* Counter is enabled */
            ENC_L_Counter_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            ENC_L_Counter_backup.CounterEnableState = 0u;
        }
    #else
        ENC_L_Counter_backup.CounterEnableState = 1u;
        if(ENC_L_Counter_backup.CounterEnableState != 0u)
        {
            ENC_L_Counter_backup.CounterEnableState = 0u;
        }
    #endif /* (!ENC_L_Counter_ControlRegRemoved) */
    
    ENC_L_Counter_Stop();
    ENC_L_Counter_SaveConfig();
}


/*******************************************************************************
* Function Name: ENC_L_Counter_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  ENC_L_Counter_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void ENC_L_Counter_Wakeup(void) 
{
    ENC_L_Counter_RestoreConfig();
    #if(!ENC_L_Counter_ControlRegRemoved)
        if(ENC_L_Counter_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            ENC_L_Counter_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!ENC_L_Counter_ControlRegRemoved) */
    
}


/* [] END OF FILE */
