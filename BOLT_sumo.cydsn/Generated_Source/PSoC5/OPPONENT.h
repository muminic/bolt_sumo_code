/*******************************************************************************
* File Name: OPPONENT.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_OPPONENT_H) /* Pins OPPONENT_H */
#define CY_PINS_OPPONENT_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "OPPONENT_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 OPPONENT__PORT == 15 && ((OPPONENT__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    OPPONENT_Write(uint8 value);
void    OPPONENT_SetDriveMode(uint8 mode);
uint8   OPPONENT_ReadDataReg(void);
uint8   OPPONENT_Read(void);
void    OPPONENT_SetInterruptMode(uint16 position, uint16 mode);
uint8   OPPONENT_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the OPPONENT_SetDriveMode() function.
     *  @{
     */
        #define OPPONENT_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define OPPONENT_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define OPPONENT_DM_RES_UP          PIN_DM_RES_UP
        #define OPPONENT_DM_RES_DWN         PIN_DM_RES_DWN
        #define OPPONENT_DM_OD_LO           PIN_DM_OD_LO
        #define OPPONENT_DM_OD_HI           PIN_DM_OD_HI
        #define OPPONENT_DM_STRONG          PIN_DM_STRONG
        #define OPPONENT_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define OPPONENT_MASK               OPPONENT__MASK
#define OPPONENT_SHIFT              OPPONENT__SHIFT
#define OPPONENT_WIDTH              1u

/* Interrupt constants */
#if defined(OPPONENT__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in OPPONENT_SetInterruptMode() function.
     *  @{
     */
        #define OPPONENT_INTR_NONE      (uint16)(0x0000u)
        #define OPPONENT_INTR_RISING    (uint16)(0x0001u)
        #define OPPONENT_INTR_FALLING   (uint16)(0x0002u)
        #define OPPONENT_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define OPPONENT_INTR_MASK      (0x01u) 
#endif /* (OPPONENT__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define OPPONENT_PS                     (* (reg8 *) OPPONENT__PS)
/* Data Register */
#define OPPONENT_DR                     (* (reg8 *) OPPONENT__DR)
/* Port Number */
#define OPPONENT_PRT_NUM                (* (reg8 *) OPPONENT__PRT) 
/* Connect to Analog Globals */                                                  
#define OPPONENT_AG                     (* (reg8 *) OPPONENT__AG)                       
/* Analog MUX bux enable */
#define OPPONENT_AMUX                   (* (reg8 *) OPPONENT__AMUX) 
/* Bidirectional Enable */                                                        
#define OPPONENT_BIE                    (* (reg8 *) OPPONENT__BIE)
/* Bit-mask for Aliased Register Access */
#define OPPONENT_BIT_MASK               (* (reg8 *) OPPONENT__BIT_MASK)
/* Bypass Enable */
#define OPPONENT_BYP                    (* (reg8 *) OPPONENT__BYP)
/* Port wide control signals */                                                   
#define OPPONENT_CTL                    (* (reg8 *) OPPONENT__CTL)
/* Drive Modes */
#define OPPONENT_DM0                    (* (reg8 *) OPPONENT__DM0) 
#define OPPONENT_DM1                    (* (reg8 *) OPPONENT__DM1)
#define OPPONENT_DM2                    (* (reg8 *) OPPONENT__DM2) 
/* Input Buffer Disable Override */
#define OPPONENT_INP_DIS                (* (reg8 *) OPPONENT__INP_DIS)
/* LCD Common or Segment Drive */
#define OPPONENT_LCD_COM_SEG            (* (reg8 *) OPPONENT__LCD_COM_SEG)
/* Enable Segment LCD */
#define OPPONENT_LCD_EN                 (* (reg8 *) OPPONENT__LCD_EN)
/* Slew Rate Control */
#define OPPONENT_SLW                    (* (reg8 *) OPPONENT__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define OPPONENT_PRTDSI__CAPS_SEL       (* (reg8 *) OPPONENT__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define OPPONENT_PRTDSI__DBL_SYNC_IN    (* (reg8 *) OPPONENT__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define OPPONENT_PRTDSI__OE_SEL0        (* (reg8 *) OPPONENT__PRTDSI__OE_SEL0) 
#define OPPONENT_PRTDSI__OE_SEL1        (* (reg8 *) OPPONENT__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define OPPONENT_PRTDSI__OUT_SEL0       (* (reg8 *) OPPONENT__PRTDSI__OUT_SEL0) 
#define OPPONENT_PRTDSI__OUT_SEL1       (* (reg8 *) OPPONENT__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define OPPONENT_PRTDSI__SYNC_OUT       (* (reg8 *) OPPONENT__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(OPPONENT__SIO_CFG)
    #define OPPONENT_SIO_HYST_EN        (* (reg8 *) OPPONENT__SIO_HYST_EN)
    #define OPPONENT_SIO_REG_HIFREQ     (* (reg8 *) OPPONENT__SIO_REG_HIFREQ)
    #define OPPONENT_SIO_CFG            (* (reg8 *) OPPONENT__SIO_CFG)
    #define OPPONENT_SIO_DIFF           (* (reg8 *) OPPONENT__SIO_DIFF)
#endif /* (OPPONENT__SIO_CFG) */

/* Interrupt Registers */
#if defined(OPPONENT__INTSTAT)
    #define OPPONENT_INTSTAT            (* (reg8 *) OPPONENT__INTSTAT)
    #define OPPONENT_SNAP               (* (reg8 *) OPPONENT__SNAP)
    
	#define OPPONENT_0_INTTYPE_REG 		(* (reg8 *) OPPONENT__0__INTTYPE)
#endif /* (OPPONENT__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_OPPONENT_H */


/* [] END OF FILE */
