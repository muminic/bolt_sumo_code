/*******************************************************************************
* File Name: sen3.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen3_H) /* Pins sen3_H */
#define CY_PINS_sen3_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen3_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen3__PORT == 15 && ((sen3__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen3_Write(uint8 value);
void    sen3_SetDriveMode(uint8 mode);
uint8   sen3_ReadDataReg(void);
uint8   sen3_Read(void);
void    sen3_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen3_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen3_SetDriveMode() function.
     *  @{
     */
        #define sen3_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen3_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen3_DM_RES_UP          PIN_DM_RES_UP
        #define sen3_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen3_DM_OD_LO           PIN_DM_OD_LO
        #define sen3_DM_OD_HI           PIN_DM_OD_HI
        #define sen3_DM_STRONG          PIN_DM_STRONG
        #define sen3_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen3_MASK               sen3__MASK
#define sen3_SHIFT              sen3__SHIFT
#define sen3_WIDTH              1u

/* Interrupt constants */
#if defined(sen3__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen3_SetInterruptMode() function.
     *  @{
     */
        #define sen3_INTR_NONE      (uint16)(0x0000u)
        #define sen3_INTR_RISING    (uint16)(0x0001u)
        #define sen3_INTR_FALLING   (uint16)(0x0002u)
        #define sen3_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen3_INTR_MASK      (0x01u) 
#endif /* (sen3__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen3_PS                     (* (reg8 *) sen3__PS)
/* Data Register */
#define sen3_DR                     (* (reg8 *) sen3__DR)
/* Port Number */
#define sen3_PRT_NUM                (* (reg8 *) sen3__PRT) 
/* Connect to Analog Globals */                                                  
#define sen3_AG                     (* (reg8 *) sen3__AG)                       
/* Analog MUX bux enable */
#define sen3_AMUX                   (* (reg8 *) sen3__AMUX) 
/* Bidirectional Enable */                                                        
#define sen3_BIE                    (* (reg8 *) sen3__BIE)
/* Bit-mask for Aliased Register Access */
#define sen3_BIT_MASK               (* (reg8 *) sen3__BIT_MASK)
/* Bypass Enable */
#define sen3_BYP                    (* (reg8 *) sen3__BYP)
/* Port wide control signals */                                                   
#define sen3_CTL                    (* (reg8 *) sen3__CTL)
/* Drive Modes */
#define sen3_DM0                    (* (reg8 *) sen3__DM0) 
#define sen3_DM1                    (* (reg8 *) sen3__DM1)
#define sen3_DM2                    (* (reg8 *) sen3__DM2) 
/* Input Buffer Disable Override */
#define sen3_INP_DIS                (* (reg8 *) sen3__INP_DIS)
/* LCD Common or Segment Drive */
#define sen3_LCD_COM_SEG            (* (reg8 *) sen3__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen3_LCD_EN                 (* (reg8 *) sen3__LCD_EN)
/* Slew Rate Control */
#define sen3_SLW                    (* (reg8 *) sen3__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen3_PRTDSI__CAPS_SEL       (* (reg8 *) sen3__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen3_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen3__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen3_PRTDSI__OE_SEL0        (* (reg8 *) sen3__PRTDSI__OE_SEL0) 
#define sen3_PRTDSI__OE_SEL1        (* (reg8 *) sen3__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen3_PRTDSI__OUT_SEL0       (* (reg8 *) sen3__PRTDSI__OUT_SEL0) 
#define sen3_PRTDSI__OUT_SEL1       (* (reg8 *) sen3__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen3_PRTDSI__SYNC_OUT       (* (reg8 *) sen3__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen3__SIO_CFG)
    #define sen3_SIO_HYST_EN        (* (reg8 *) sen3__SIO_HYST_EN)
    #define sen3_SIO_REG_HIFREQ     (* (reg8 *) sen3__SIO_REG_HIFREQ)
    #define sen3_SIO_CFG            (* (reg8 *) sen3__SIO_CFG)
    #define sen3_SIO_DIFF           (* (reg8 *) sen3__SIO_DIFF)
#endif /* (sen3__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen3__INTSTAT)
    #define sen3_INTSTAT            (* (reg8 *) sen3__INTSTAT)
    #define sen3_SNAP               (* (reg8 *) sen3__SNAP)
    
	#define sen3_0_INTTYPE_REG 		(* (reg8 *) sen3__0__INTTYPE)
#endif /* (sen3__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen3_H */


/* [] END OF FILE */
