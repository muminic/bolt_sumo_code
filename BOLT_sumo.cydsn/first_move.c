/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "first_move.h"
#include "sensors.h"
#include "timer_time.h"
#include "drive.h"
#include "stdlib.h"
#include "flag_servo.h"
#include "drive_smart.h"

//=============================================================================
// Private defines and typedefs
//=============================================================================
#define Rd_bits(value, mask)        ((value)&(mask))

#define SEARCH_PWM_L  25
#define SEARCH_PWM_R  25
#define TURN_PWM    40
#define ATTACK_PWM  70


void firstMoveTurnRight();
void firstMoveTurnLeft();
void firstMoveFastForward();
void firstMoveDoubleFastForward();
void firstMoveDriveToLine();
void firstMoveDriveSlowToLine();
void firstMoveTurnRightARC();
void firstMoveTurnLeftARC();
void firstMoveArcLeft();

void firstMoveSelector(FirstMove_t firstMove)
{

    switch(firstMove)
    {
        case DEFAULT:
            drive_forward(SEARCH_PWM_L, SEARCH_PWM_R);
            CyDelay(50);
        break;
        case FAST_FORWARD:
            firstMoveFastForward();
        break;
        case TURN_RIGHT_45:
            firstMoveTurnRight();
        break;
        case TURN_LEFT_45:
            firstMoveTurnLeft();
        break;
        case RIGHT_LINE_ARC:
            firstMoveRightLineArc();
        break;
        case DOUBLE_FAST_FORWARD:
            firstMoveDoubleFastForward();
        break;
        case DRIVE_TO_LINE:
            firstMoveDriveToLine();
        break;
        case DRIVE_SLOW_TO_LINE:
            firstMoveDriveSlowToLine();
            break;
        case ARC_LEFT_30:
            firstMoveTurnRightARC();
            break;
        case ARC_RIGHT_30:
            firstMoveTurnLeftARC();
            break;
        case ARC_LEFT_40:
            firstMoveArcLeft();
            break;
        default:
            drive_forward(SEARCH_PWM_L, SEARCH_PWM_R);
        break;
    }
}

void firstMoveRightLineArc() //not used
{
    //driveLineRight(1300);
    drive_arcLeftPID(45,90,80);
    turn_left(50,70);
    CyDelay(250);
}

void firstMoveTurnRight()
{
    turn_right(40,40);
    CyDelay(50);    
}

void firstMoveTurnLeft()
{
    turn_left(40,40);
    CyDelay(50);    
}
void firstMoveFastForward()
{
    drive_forward(ATTACK_PWM, ATTACK_PWM);
    CyDelay(280);
    brake();
    CyDelay(30);
    drive_forward(0,0);   
}

void firstMoveDoubleFastForward()
{
    drive_forward(ATTACK_PWM, ATTACK_PWM);
    CyDelay(450);
    brake();
    CyDelay(30);
    drive_forward(ATTACK_PWM, ATTACK_PWM);
    CyDelay(40);
    brake();
    CyDelay(30);
    drive_forward(0,0);   
}

void firstMoveDriveToLine()
{
    drive_forward(ATTACK_PWM, ATTACK_PWM);
    CyDelay(300);
    while(LineSensRead() == 0){
        drive_forward(40,40);
    }   
}

void firstMoveDriveSlowToLine()
{
    drive_forward(30, 30);
    CyDelay(300);
    while(LineSensRead() == 0){
        drive_forward(25,25);
    }   
}

void firstMoveTurnRightARC()
{
    drive_turnRight_s(65,50);
    drive_waitFor_DONE();
    drive_arcLeftPID(40,80,80);
    drive_waitFor_DONE();
}

void firstMoveTurnLeftARC()
{
    drive_turnLeft_s(80,50);
    drive_waitFor_DONE();
    drive_arcRightPID(40,80,80);
    drive_waitFor_DONE();
}

void firstMoveArcLeft()
{
    drive_arcLeftPID(45,80,80);
    drive_waitFor_DONE();
}


