/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "timer_time.h"

//=============================================================================
// Private defines and typedefs
//=============================================================================
#define TIMER_TC_VALUE      60000u  // 60 sec

//=============================================================================
// Private function declarations
//=============================================================================
CY_ISR_PROTO(timer_tc_handler);

//=============================================================================
// Private data definitions
//=============================================================================
static uint32_t round_minutes = 0u; // one minute is 60000 milliseconds

//=============================================================================
// Public function definitions
//=============================================================================
void time_start(void)
{
    TIMER_MS_WriteCounter(TIMER_TC_VALUE);
    TIMER_MS_Start();
    isr_60sec_StartEx(timer_tc_handler);
}

uint32_t millis(void)
{
    // timer actually counts down from TIMER_TC_VALUE to zero
    return round_minutes + (TIMER_TC_VALUE - TIMER_MS_ReadCounter());
}

//=============================================================================
// Private function definitions
//=============================================================================
CY_ISR(timer_tc_handler)
{
    round_minutes += TIMER_TC_VALUE;
    TIMER_MS_ReadStatusRegister();
    isr_60sec_ClearPending();
}

/* [] END OF FILE */
