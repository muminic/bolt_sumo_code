/* ========================================
 *
 * Copyright ARMANDS UPENIEKS , 2018
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "stdlib.h"

#include "ssd1306.h"
#include "drive.h"
#include "timer_time.h"
#include "sensors.h"


#include "menu.h"
#include "fight_menu.h"
#include "tests.h"
#include "sensor_test.h"

#include "settings.h"
#include "flag_servo.h"

#define DISPLAY_ADDRESS 0x3C // 011110+SA0+RW - 0x3C or 0x3D


const char mainMenu_title[] = " #MINI#";

const char mainMenu_1[] = "1.FIGHT";
const char mainMenu_2[] = "2.SENSORS";
const char mainMenu_3[] = "3.TESTS";
const char mainMenu_4[] = "4.SETTINGS";

const char* mainMenu_strings[] = {mainMenu_1, mainMenu_2, mainMenu_3, mainMenu_4 };


int main(void)
{
    flagServo_Init();
    Motor_Init();
    I2COLED_Start();
    //ENC_L_Counter_Start();
    //ENC_R_Counter_Start();
    RIGHT_ENC_Start();
    LEFT_ENC_Start();
    CyGlobalIntEnable; /* Enable global interrupts. */
    //USBUART_Start(0u, USBUART_3V_OPERATION);
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    CyDelay(100);
    display_init(DISPLAY_ADDRESS);
    time_start();
    ADC_SAR_Start();
    ADC_SAR_StartConvert();
    EEPROM_Start();
    sensorsInit();
    
    
    
    ReadEEPROMSettings(); //Read global variables from EEPROM
    
    if (1u == IR_START_Read()) {
        gSettings.lastState = 1;
        fastFight();    
    }
    
    showText (25,"BOLT SUMO");
    CyDelay(1000);
    Servo_enable_Write(1);
    servoAngle(87); 
    CyDelay(150);
    Servo_enable_Write(0);

    Menu MainMenu = {mainMenu_title, mainMenu_strings, 4, 0, 0};
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
    for(;;)
    {
        //drive_forward(30,30);
        int menuStatus = ShowMenu(&MainMenu);
        CyDelay(200);
        drive_forward(0,0);
        
		switch( menuStatus ){
			case 0:
                tacticPreSelector();
				break;
			case 1:  
				SensorMenu();
				break;
			case 2:  
				TestsMenu();
				break;
            case 3:  
				SettingsMenu();
				break;
		}
        /* Place your application code here. */
    //opponentSensors(); 
    //lineSensors();
    //RunTactic1();
    //turn_left(20, 20);
    //test();
    //drive_forward(40,40);
    //encoderSensors();
    //drive_distance_straight_forward(19, 40);
    //drive_forward(0,0);
    //CyDelay(3000);
        
    }
}

/* [] END OF FILE */
