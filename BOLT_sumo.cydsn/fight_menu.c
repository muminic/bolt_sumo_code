/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "fight_menu.h"
#include "first_move.h"
#include "tactic0_basic.h"
#include "tactic1_Nooo.h"
#include "tactic2_blitz.h"
#include "tactic3_scary.h"
#include "tactic4_ND.h"
#include "tactic5_ND_blitz.h"
#include "tactic7.h"
#include "tactic8_trr.h"
#include "tactic9_ND_TicTac.h"
#include "settings.h"
#include "flag_servo.h"

//#include "tactic_test.h"
#include "stdint.h"
#include "menu.h"
#include "ssd1306.h"
#include "stdlib.h"

#define Rd_bits(value, mask)        ((value)&(mask))

#define TacticNumber 4

const char preFightMenu_title[] = " #servo";

const char preFightMenu_1[] = "1. LEFT <-";
const char preFightMenu_2[] = "2. UP ||";
const char preFightMenu_3[] = "3. RIGHT ->";

const char* preFightMenu_strings[] = {preFightMenu_1, preFightMenu_2, preFightMenu_3}; 

const char fightMenu_title[] = " #F";

const char fightMenu_1[] = "1.BASIC";
const char fightMenu_2[] = "2.BLITZ"; 
const char fightMenu_3[] = "3.SlowBasic"; 
const char fightMenu_4[] = "4.// ARC25"; 
const char fightMenu_5[] = "5.BLITZ R45"; 
const char fightMenu_6[] = "6.|> ARC30"; 
const char fightMenu_7[] = "7.|< ARC30"; 
const char fightMenu_8[] = "8.fast forward"; 
const char fightMenu_9[] = "9.nO Delay"; 
const char fightMenu_10[] = "10.ND Blitz"; 
const char fightMenu_11[] = "11.2xFastForwar"; 
const char fightMenu_12[] = "12.ForwardToLin";  
const char fightMenu_13[] = "13.stupido"; 
const char fightMenu_14[] = "14.SlowToLin"; 
const char fightMenu_15[] = "15.NDTicTac";
const char fightMenu_16[] = "16.LINER";


const char* fightMenu_strings[] = {fightMenu_1, fightMenu_2, fightMenu_3, fightMenu_4, fightMenu_5, fightMenu_6, 
    fightMenu_7, fightMenu_8, fightMenu_9, fightMenu_10, fightMenu_11, fightMenu_12, fightMenu_13, fightMenu_14,
    fightMenu_15,fightMenu_16 };


void tacticPreSelector(){
    Menu PreFightMenu = {preFightMenu_title, preFightMenu_strings, 3, 0, 0};
    
		// (Re)Initialize menu system and wait for menu item selection.
    int tactic = ShowMenu(&PreFightMenu); 
    switch (tactic) {
                case 0:
                ServoPosition = 1;
                tacticSelector();
                break;
                case 1:
                ServoPosition = 0;
                tacticSelector();
                break;
                case 2:
                ServoPosition = 2;
                tacticSelector();
                break;
    }
}

void tacticSelector(){
    Menu FightMenu = {fightMenu_title, fightMenu_strings, 16, 0, 0};
    
		// (Re)Initialize menu system and wait for menu item selection.
    int tactic = ShowMenu(&FightMenu); 
    
    char text[5];
    itoa(tactic, text, 10);
      
        display_clear(); 
        gfx_setTextSize(2);
        gfx_setTextColor(WHITE);
        gfx_setTextBg(BLACK);
        gfx_setCursor(5,1);
        gfx_print("FIGHT"); 
        display_update();
        display_invert(1);
        gSettings.lastState = 0;
                   
        switch (tactic) {
            case 0:
            gSettings.lastTactic = 0;
            RunTactic0(DEFAULT, 75);
            break;
            case 1:
            //gSettings.lastTactic = 1;
            //RunTactic1();
            gSettings.lastTactic = 2;
            RunTactic2(DEFAULT);
            break;
            case 2:
            gSettings.lastTactic =5;
            RunTactic0(DEFAULT, 35);
            break;
            case 3:
            //gSettings.lastTactic = 3;
            //RunTactic3();
            RunTactic0(ARC_LEFT_40, 75);
            break;
            case 4:
            gSettings.lastTactic = 4;
            RunTactic2(TURN_RIGHT_45);
            break;
            case 5:
            gSettings.lastTactic = 0;
            //RunTactic0(TURN_RIGHT_45, 75);
            RunTactic0(ARC_LEFT_30, 75);
            break;
            case 6:
            gSettings.lastTactic = 0;
            RunTactic0(ARC_RIGHT_30, 75);
            //RunTactic0(TURN_LEFT_45, 75);
            break;
            case 7:
            gSettings.lastTactic = 0;
            RunTactic0(FAST_FORWARD, 75);
            break;
            case 8:
            gSettings.lastTactic =4;
            RunTactic4();
            break;
            case 9:
            gSettings.lastTactic =5;
            RunTactic5();
            break;
            case 10:
            gSettings.lastTactic =5;
            RunTactic0(DOUBLE_FAST_FORWARD, 75);
            break;
            case 11:
            gSettings.lastTactic =5;
            RunTactic0(DRIVE_TO_LINE, 75);
            break;
            case 12:
            gSettings.lastTactic =5;
            RunTactic7(FAST_FORWARD);
            break;
            case 13:
            gSettings.lastTactic =5;
            RunTactic0(DRIVE_SLOW_TO_LINE, 75);
            break;
            case 14:
            gSettings.lastTactic =5;
            RunTactic9();
            break;
            case 15:
            gSettings.lastTactic =5;
            RunTactic8();
            break;
        } 
}

void fastFight(void){
    switch (gSettings.lastTactic) {
        case 0:
            gSettings.lastTactic = 0;
            RunTactic0(0,75);
        break;
        case 1:
            gSettings.lastTactic = 1;
            RunTactic1();
        break;
        case 2:
            gSettings.lastTactic = 2;
            RunTactic2(DEFAULT);
        break;
        case 3:
            gSettings.lastTactic = 3;
            RunTactic3();
        break;
        case 4:
            gSettings.lastTactic = 4;
            RunTactic4();
        break;
        case 5:
            gSettings.lastTactic = 5;
            RunTactic0(0,75);
        break;
        case 6:
            gSettings.lastTactic = 6;
            RunTactic0(0,75);
        break;
        case 7:
            gSettings.lastTactic = 7;
            RunTactic0(FAST_FORWARD,75);
        break;
    }    
}
/* [] END OF FILE */
