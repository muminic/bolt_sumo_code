/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

void SensorMenu();
void opponentSensors(void);
void lineSensors(void);
void encoderSensors(void);
void UpSensors();
void AnalogSensors(void);
void test(void);

/* [] END OF FILE */
