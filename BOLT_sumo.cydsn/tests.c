/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "tests.h"
#include "project.h"
#include "stdio.h"
#include "stdlib.h"
#include "ssd1306.h"
#include "menu.h"
#include "drive.h"
#include "sensors.h"
#include "drive_smart.h"

#define Rd_bits(value, mask)        ((value)&(mask))
#define TURN_PWM 35

const char testsMenu_title[] = " #TES#";
// Menu Page1
const char testsMenu_1[] = "1.Motor test";
const char testsMenu_2[] = "2.Line test";
const char testsMenu_3[] = "3.Track Object";
const char testsMenu_4[] = "4.Drive BackTill";
const char testsMenu_5[] = "5.DriveEncoder";
// Menu Page2
const char testsMenu_6[] = "6.EXIT";

const char* testsMenu_strings[] = {testsMenu_1, testsMenu_2, testsMenu_3,
	testsMenu_4,testsMenu_5,testsMenu_6,};

void ActOnLineDetected(void);
void trackObject(void);
void motorTest(void);
void lineTest(void);
void driveEncoders(void);




void TestsMenu(){
    Menu testsMenu = {testsMenu_title, testsMenu_strings, 6, 0, 0};
    while( 1 ){
		// (Re)Initialize menu system and wait for menu item selection.
		int menuStatus = ShowMenu(&testsMenu);

		switch( menuStatus ){
			case 0:  
				motorTest();         
				break;
			case 1:  
				lineTest();
				break;
			case 2:  // 3.encoders
				trackObject();
				break;
            case 3:  // 3.encoders
				drive_backwardTillLineSpotted(40);
                break;
            case 4:  // 3.encoders
				driveEncoders();
                break;
			case 5:  
			default:
				// Return to the main menu
				return;
		}
	}
}

void motorTest(){
    drive_backward(30,30);
    showText(25,"Motor Test");
    while (!GoBack()) {
        //drive forward    
    }
    drive_forward(0,0);
}


void driveEncoders()
{
    
    //drive_arcLeftPID(30,180,80);
    drive_turnRight_s(50,30);
    drive_waitFor_DONE();
    //drive_forward_s(60,30);
    drive_arcLeftPID(30,180,80);
    drive_waitFor_DONE();
    //drive_turnLeft_s(90,30);
}

void lineTest(){
    int lineSens = 0;
    showText(25,"line test");
    while (!GoBack()) {
	    lineSens = LineSensRead();

		if( 0 < lineSens ){
			// line has detected
		    ActOnLineDetected();
		}
        else {
            drive_forward(gMotor.basic_speed, gMotor.basic_speed); //SEARCH_PWM_L SEARCH_PWM_R
        }
    }
    drive_forward(0,0);
    
}

void trackObject(void){
    showText(20,"track object");
    while (!GoBack()) {
	    int distSens = DistSensRead(); //Distance_sensor_value_Read();
        //Sensor_Reset_Write(0xFF);  // why???
        
        //Sensor_Reset_Write(0); 

    	if( 0 < distSens ){
            int8_t avrg_angle_grad = CalcAvrgAngle();

        	int8_t pwm = 60;
            #define ATTACK_PWM 0
        	// Attack spotted opponent
        	if( 0 == avrg_angle_grad ){
        		drive_forward( ATTACK_PWM , ATTACK_PWM );		// drive forward
        		CyDelay(40);
        	}else if( -60 >= avrg_angle_grad ){
        		// [-90; -60] grad
        		turn_left(TURN_PWM,TURN_PWM); //drive_motors( -pwm, pwm );		// turn on spot left
                CyDelay(40);
        	}else if( -30 >= avrg_angle_grad ){
        		// (-60; -30] grad
        		drive_forward( pwm / 3, pwm );	// drive more left
                CyDelay(30);
        	}else if( 0 > avrg_angle_grad ){
        		// (-30; 0) grad
        		drive_forward( ATTACK_PWM * 0.7, ATTACK_PWM );	// drive little left
                CyDelay(40);
        	}else if( 60 <= avrg_angle_grad ){
        		// [60; 90] grad
        		turn_right(TURN_PWM,TURN_PWM); //drive_motors( pwm, -pwm );		// turn on spot right
                CyDelay(40);
        	}else if( 30 <= avrg_angle_grad ){
        		// [30; 60) grad
        		drive_forward( pwm, pwm / 3 );	// drive more right
                CyDelay(30);
        	}else if( 0 <= avrg_angle_grad ){
        		// (0; 30) grad
        		drive_forward( ATTACK_PWM, ATTACK_PWM * 0.7 );	// drive little right
                CyDelay(40);
        	}
            CyDelay(10);

    		}
        else {
            drive_forward(0, 0);
            //CyDelay(50);
        }
    }       
}

void ActOnLineDetected(void){
	//int lineSens = LineSensRead();
     
	// Line_FL && Line_FR
	if(gLine.F_LEFT && gLine.F_RIGHT){
        drive_backward(40,40); //drive_distance_straight_backward(10, 40);
        CyDelay(400);
        //turn_angle_right(170,50);
        turn_left(TURN_PWM,TURN_PWM); //turn_angle_right(180, TURN_PWM );
        CyDelay(300);
	}
	// Line_FL
	else if(gLine.F_LEFT){
        drive_backward(40,40); //drive_distance_straight_backward(10, 40);
        CyDelay(150);
        turn_right(TURN_PWM,TURN_PWM); //turn_angle_right(135, TURN_PWM );
        CyDelay(200);
	}
	// Line_FR
	else if(gLine.F_RIGHT){
        drive_backward(40,40); //drive_distance_straight_backward(10, 40);
        CyDelay(150);
        turn_left(TURN_PWM,TURN_PWM); //turn_angle_left(135, TURN_PWM );
        CyDelay(200);
	}
    //drive_forward( 0, 0 );
}


/* [] END OF FILE */
