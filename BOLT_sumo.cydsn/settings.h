/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "stdio.h"
#include "stdlib.h"



#define EEPROM_ADDR_LINE_DETECT 2
#define EEPROM_ADDR_BASIC_SPEED 4
#define EEPROM_ADDR_TURN_SPEED 6
#define EEPROM_ADDR_DIST_SEN_DISENABLE 8
#define EEPROM_ADDR_FIVE_SECOND_DELAY 10
#define EEPROM_ADDR_FLAG 12
#define EEPROM_ADDR_LAST_STATE 14
#define EEPROM_ADDR_LAST_TACTIC 16
#define EEPROM_ADDR_ANALOG_TRESHHOLD 18
#define EEPROM_ADDR_LINE_SENSOR_MASK 20
#define EEPROM_ADDR_OPPONENT_SENSOR_MASK 22


 struct settings{
    int8_t five_seconds;
    int8_t flag;
    int turn_speed;
    int lastState;
	int lastTactic;
    int analogTreshhold;
    
}gSettings;


void ReadEEPROMSettings(void);
void SettingsMenu();



/* [] END OF FILE */
