/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef TACTIC_X_H
#define TACTIC_X_H

//=============================================================================
// Public function declarations
//=============================================================================
void RunTacticX(int firstMove,int speedlimit);

#endif /* TACTIC8_H */
/* [] END OF FILE */
