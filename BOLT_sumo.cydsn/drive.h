/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/


#ifndef DRIVE_H
#define DRIVE_H
    
 struct gDrivevars{
    int basic_speed;
    int turn_speed;
    int max_pwm_Limited_L;
    int max_pwm_Limited_R;
	
}gMotor;

void Motor_Init(void);
void motors_drive(int left_speed, int right_speed);
void drive_forward(int left_pwm, int right_pwm);
void drive_backward(int left_pwm, int right_pwm);
void turn_right(int left_pwm, int right_pwm);
void turn_left(int left_pwm, int right_pwm);
void motors_brake(void);
void brake(void);
void drive_distance_straight_forward(int distance, int speed);
void turn_angle_right(int angle, int speed);
void turn_angle_left(int angle, int speed);
void drive_right_arc_forward(int distance, int speed);
void drive_backwardTillLineSpotted (int speed);

#endif /* DRIVE_H */