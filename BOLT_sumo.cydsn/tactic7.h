/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef TACTIC_7_H
#define TACTIC_7_H

//=============================================================================
// Public function declarations
//=============================================================================
void RunTactic7(int firstMove);

#endif /* TACTIC1_H */
/* [] END OF FILE */