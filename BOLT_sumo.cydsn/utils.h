/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef UTILS_H
#define UTILS_H
// \brief Convenience macro for counting elements in arrays
#define ARRAY_SIZE(x)                       (sizeof(x) / sizeof((x)[0]))

#define Rd_bits(value, mask)                ((value) & (mask))
#define Clr_bits(value, mask)               ((value) &= ~(mask))
#define Set_bits(value, mask)               ((value) |= (mask))

#define bitRead(value, bit)                 (((value) >> (bit)) & 0x01)
#define bitSet(value, bit)                  ((value) |= (1UL << (bit)))
#define bitClear(value, bit)                ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue)      (bitvalue ? bitSet(value, bit) : bitClear(value, bit))
#endif /* UTILS_H */
