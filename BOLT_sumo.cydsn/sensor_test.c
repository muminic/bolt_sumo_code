/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "sensor_test.h"
#include "project.h"
#include "stdio.h"
#include "stdlib.h"
#include "ssd1306.h"
#include "menu.h"
#include "drive.h"
#include "flag_servo.h"

#define Rd_bits(value, mask)        ((value)&(mask))


const char sensorsMenu_title[] = "_";
// Menu Page1
const char sensorsMenu_1[] = "1.Oppon Sensors";
const char sensorsMenu_2[] = "2.Line Sensors";
const char sensorsMenu_3[] = "3.Encoders";
const char sensorsMenu_4[] = "4.Servo Right";
const char sensorsMenu_5[] = "5.Servo Left";
const char sensorsMenu_6[] = "6.Servo Up";
const char sensorsMenu_7[] = "7.EXIT";

// Menu Page2


const char* sensorsMenu_strings[] = {sensorsMenu_1, sensorsMenu_2, sensorsMenu_3,
	sensorsMenu_4, sensorsMenu_5, sensorsMenu_6, sensorsMenu_7};



void SensorMenu(){
    Menu SensorsMenu = {sensorsMenu_title, sensorsMenu_strings, 7, 0, 0};
    while( 1 ){
		// (Re)Initialize menu system and wait for menu item selection.
		int menuStatus = ShowMenu(&SensorsMenu);

		switch( menuStatus ){
			case 0:  
				opponentSensors();         
				break;
			case 1:  
				lineSensors();
				break;
			case 2:  // 3.encoders
				encoderSensors();
				break;
            case 3:  // Servo Right
                Servo_enable_Write(1);
				rightFlagDown();
				break;
            case 4:  // Servo Left
                Servo_enable_Write(1);
				leftFlagDown();
				break;
            case 5:  // 6.Servo Up
                Servo_enable_Write(1);
				FlagUp();
				break;
			case 6:
			default:
				// Return to the main menu
				return;
		}
	}
}



void opponentSensors(void){
    char squareSize = 6;
    char fillSquareSize = squareSize - 2;
   
    display_clear();
    
    gfx_drawRect(0,10,squareSize,squareSize,1); //left
    gfx_drawRect(0,2,squareSize,squareSize,1);
    gfx_drawRect(30,0,squareSize,squareSize,1);
    gfx_drawRect(45,1,squareSize,squareSize,1); //center
    gfx_drawRect(60,0,squareSize,squareSize,1);
    gfx_drawRect(95 - squareSize, 2,squareSize,squareSize,1);
    gfx_drawRect(95 - squareSize, 10, squareSize, squareSize,1); //right    
    
    while (!GoBack()) {
        if( 0u == IRL_L90_Read() ){
            gfx_fillRect(1,11,fillSquareSize,fillSquareSize,1);
	    }
        else {
            gfx_fillRect(1,11,fillSquareSize,fillSquareSize,0);
        }
        
    	if( 0u == IRL_L60_Read() ){
	        gfx_fillRect(1,3,fillSquareSize,fillSquareSize,1);	
	    }
        else {
            gfx_fillRect(1,3,fillSquareSize,fillSquareSize,0);
        }
    	if( 0u == IRL_L30_Read() ){
    		gfx_fillRect(31,1,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(31,1,fillSquareSize,fillSquareSize,0);
        }
    	
    	if( 0u == IRL_R0_Read() ){
    		gfx_fillRect(46,2,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(46,2,fillSquareSize,fillSquareSize,0);
        }
    	if( 0u == IRL_R30_Read() ){
    		gfx_fillRect(61,1,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(61,1,fillSquareSize,fillSquareSize,0);
        }
    	if( 0u == IRL_R60_Read() ){
    		gfx_fillRect(96 - squareSize,3,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(96 - squareSize,3,fillSquareSize,fillSquareSize,0);
        }
    	if( 0u == IRL_R90_Read() ){
    		gfx_fillRect(96 - squareSize,11,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(96 - squareSize,11,fillSquareSize,fillSquareSize,0);
        }
         display_update();

       CyDelay(100); 
    }
}


void encoderSensors(void){
   
    RIGHT_ENC_SetCounter(0);
    LEFT_ENC_SetCounter(0);
    
    drive_forward(20,20);
    
     while (!GoBack()) {
        display_clear(); 
        gfx_setTextSize(1);
        gfx_setTextColor(WHITE);
        gfx_setTextBg(BLACK);
      

        int enc_r = RIGHT_ENC_GetCounter();
        int enc_l = LEFT_ENC_GetCounter();

        if (enc_r >=28) motors_brake();
        
        char text2[5];
        char text3[5];

        itoa(enc_l, text2, 10);
        itoa(enc_r, text3, 10);
        
        gfx_setCursor(80,0);
        gfx_print(text3); 
        gfx_print("   ");
        gfx_setCursor(25,0);
        gfx_print(text2);
        gfx_print("   ");

        display_update();
        CyDelay(10);
    }
    drive_forward(0,0);
}

void lineSensors(void){
   
    display_clear(); 
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
        
    while (!GoBack()) {
        int16 lineFL1 = ADC_SAR_GetResult16(0);
        int16 lineFL2 = ADC_SAR_GetResult16(1);
        
        int16 lineFR1 = ADC_SAR_GetResult16(4);
        int16 lineFR2 = ADC_SAR_GetResult16(3);

        
        int16 lineBack = ADC_SAR_GetResult16(6);
        char text1[5];
        char text2[5];
        char text3[5];
        char text4[5];
        char text5[5];

        itoa(lineFL1, text1, 10);
        itoa(lineFL2, text2, 10);
        itoa(lineFR1, text3, 10);
        itoa(lineFR2, text4, 10);
        itoa(lineBack, text5, 10);
         
        gfx_setCursor(70,0);
        gfx_print(text3); 
        gfx_print("   ");
        gfx_setCursor(0,0);
        gfx_print(text1);
        gfx_print("   ");
        gfx_setCursor(0,8);
        gfx_print(text2);
        gfx_print("   ");
        gfx_setCursor(70,8);
        gfx_print(text4);
        gfx_print("   ");
        gfx_setCursor(30,8);
        gfx_print(text5);
        gfx_print("   ");

        
        display_update();
        CyDelay(500);
    }
}

void UpSensors(void){
   
    display_clear(); 
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
        
    while (!GoBack()) {
        int16 up1 = ADC_SAR_GetResult16(2);
        int16 up2 = ADC_SAR_GetResult16(5);

        char text1[5];
        char text2[5];

        itoa(up1, text1, 10);
        itoa(up2, text2, 10);

        gfx_setCursor(0,0);
        gfx_print(text1);
        gfx_setCursor(0,8);
        gfx_print(text2);

        display_update();
        CyDelay(500);
    }
}

void AnalogSensors(void){
   
    display_clear(); 
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
        
    while (!GoBack()) {
        int16 analog = ADC_SAR_GetResult16(7);

        char text1[5];

        itoa(analog, text1, 10);

        gfx_setCursor(0,0);
        gfx_print(text1);

        display_update();
        CyDelay(500);
    }
}

void test(void){
    char squareSize = 8;
    char fillSquareSize = squareSize - 2;
   
    display_clear();
    
    gfx_drawRect(0,23,squareSize,squareSize,1); //left
    gfx_drawRect(0,5,squareSize,squareSize,1);
    gfx_drawRect(40,0,squareSize,squareSize,1);
    gfx_drawRect(60,3,squareSize,squareSize,1); //center
    gfx_drawRect(80,0,squareSize,squareSize,1);
    gfx_drawRect(127 - squareSize, 5,squareSize,squareSize,1);
    gfx_drawRect(127 - squareSize, 23, squareSize, squareSize,1); //right    
    
        
        
    	if( 0u == poga_1_Read() ){
	        gfx_fillRect(1,6,fillSquareSize,fillSquareSize,1);	
	    }
        else {
            gfx_fillRect(1,6,fillSquareSize,fillSquareSize,0);
        }
    	if( 0u == poga_2_Read() ){
    		gfx_fillRect(41,1,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(41,1,fillSquareSize,fillSquareSize,0);
        }
    	
    	if( 0u == poga_3_Read() ){
    		gfx_fillRect(61,4,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(61,4,fillSquareSize,fillSquareSize,0);
        }
    	if( 0u == IR_START_Read() ){
    		gfx_fillRect(81,1,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(81,1,fillSquareSize,fillSquareSize,0);
        }
    	if( 0u == ex_5_Read() ){
    		gfx_fillRect(128 - squareSize,6,fillSquareSize,fillSquareSize,1);
    	}
        else {
            gfx_fillRect(128 - squareSize,6,fillSquareSize,fillSquareSize,0);
        }
    	
        
        
        display_update();
        
        
        
        
        
       CyDelay(100); 
    
    

}

/* [] END OF FILE */
