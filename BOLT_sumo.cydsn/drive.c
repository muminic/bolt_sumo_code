/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "drive.h"
#include "project.h"
#include "sensors.h"

#define TICKS_PER_REVOLUTION 5
#define MAX_PWM     98
#define MIN_PWM     5
#define CM_TO_TICKS_SCALE  2 //1.5  // need to calibrate
#define ANGLE_TO_TICKS_SCALE 0.06 // need to calibrate

void Motor_Init(void){
    MOT_PWM_Start();
    MOT_CTRL_reg_Write(0); 
	MOT_CTRL_reg_1_Write(0);
}

void motors_drive(int left_speed, int right_speed)
{
if (left_speed >=0 && right_speed >=0)
    {drive_forward(left_speed, right_speed);}
else if (left_speed >=0)
{
    {turn_right(left_speed, right_speed);}
}
else if (right_speed >=0)
{
    {turn_left(left_speed, right_speed);}
}
else {
    drive_backward(left_speed, right_speed);
}
}


void drive_backward(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x8); 
	MOT_CTRL_reg_1_Write(0x1);
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
}

void soft_drive_forward(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x4); 
	MOT_CTRL_reg_1_Write(0x2);
    while(right_pwm != gMotor.max_pwm_Limited_R && left_pwm != gMotor.max_pwm_Limited_L ) {
		// Soft start implementation
		if( left_pwm > gMotor.max_pwm_Limited_L ){
			gMotor.max_pwm_Limited_L += 5;
			if( left_pwm < gMotor.max_pwm_Limited_L ){
				gMotor.max_pwm_Limited_L = left_pwm;
			}
		}
		if( right_pwm > gMotor.max_pwm_Limited_R ){
			gMotor.max_pwm_Limited_R += 5;
			if( right_pwm < gMotor.max_pwm_Limited_R ){
				gMotor.max_pwm_Limited_R = right_pwm;
			}
		}        
        CyDelay(50);
        MOT_PWM_WriteCompare1(gMotor.max_pwm_Limited_L);
        MOT_PWM_WriteCompare2(gMotor.max_pwm_Limited_R);   
    }
    MOT_PWM_WriteCompare1(gMotor.max_pwm_Limited_L);
    MOT_PWM_WriteCompare2(gMotor.max_pwm_Limited_R);
    
}

void drive_forward(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x4); 
	MOT_CTRL_reg_1_Write(0x2);
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    
}

void turn_left(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x8); 
	MOT_CTRL_reg_1_Write(0x2);
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
}

void turn_right(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x4); 
	MOT_CTRL_reg_1_Write(0x1);
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
}

void motors_brake(void){
    MOT_CTRL_reg_Write(0xF); 
	MOT_CTRL_reg_1_Write(0xF);
    MOT_PWM_WriteCompare1(0);
    MOT_PWM_WriteCompare2(0);
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
    CyDelay(10);
}

void brake(void){
    motors_brake();
}






/* [] END OF FILE */
