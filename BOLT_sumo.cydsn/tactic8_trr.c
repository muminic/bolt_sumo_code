/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "tactic8_trr.h"
#include "tactic4_ND.h"
#include "sensors.h"
#include "timer_time.h"
#include "drive.h"
#include "first_move.h"

//=============================================================================
// Private defines and typedefs
//=============================================================================
#define Rd_bits(value, mask)        ((value)&(mask))

#define SEARCH_PWM_L  45
#define SEARCH_PWM_R  45
#define TURN_PWM    45
#define ATTACK_PWM  80

//=============================================================================
// Private function declarations
//=============================================================================
static void ActOnEnemySpotted(void);
static void ActOnLineDetected(void);
static void DoSomething(void);
static void setDrivePwmNow(DriveDirections_t dir, int pwmL, int pwmR, unsigned long duration);
static void setDrivePwmNext(DriveDirections_t dir, int pwmL, int pwmR, unsigned long duration);
static void handleDrive();


//=============================================================================
// Private data definitions
//=============================================================================
static uint8_t lineSens = 0;
static uint8_t distSens = 0;

static unsigned long currentDriveEndTime;
static int8 nextDriveActive = 0;
static DriveDirections_t nextDirection = DRIVE_STOP;
static int nextPWML = 0;
static int nextPWMR = 0;
static unsigned long nextDriveDuration = 0;


//=============================================================================
// Public function definitions
//=============================================================================

//-----------------------------------------------------------------------------
//! \brief	Combat tactic #1 in action
//-----------------------------------------------------------------------------

void setDrivePwmNow(DriveDirections_t dir, int pwmL, int pwmR, unsigned long duration){
  
  switch(dir){
    case DRIVE_STOP:
      drive_forward(0, 0);
    break;
    case DRIVE_FORWARD:
        drive_forward(pwmL, pwmR);
    break;  
    case DRIVE_BACKWARD:
      drive_backward(pwmL, pwmR);
    break;  
    case DRIVE_TURN_LEFT:
        turn_left(pwmL, pwmR);
    break; 
    case DRIVE_TURN_RIGHT:
        turn_right(pwmL, pwmR);
    break; 
  }
  
  currentDriveEndTime = millis() + duration;
}

void setDrivePwmNext(DriveDirections_t dir, int pwmL, int pwmR, unsigned long duration){
  nextDirection = dir;
  nextPWML = pwmL;
  nextPWMR = pwmR;
  nextDriveDuration = duration;
  nextDriveActive = 1;
}

void handleDrive(){    
  if(millis() >= currentDriveEndTime){    // ja ir pagājis tekošās braukšanas komandas laiks     
    if(nextDriveActive == 1){
      setDrivePwmNow(nextDirection, nextPWML, nextPWMR, nextDriveDuration);
      nextDriveActive = 0;
    }else{
      setDrivePwmNow(DRIVE_FORWARD, SEARCH_PWM_L, SEARCH_PWM_R, 50);      // liekam robotam braukt taisni, ja nav uzdota neviena braukšanas komanda  
    }    
  }  
}

void RunTactic8()
{
    WaitForIRStart();
    
    // ============================ First move ================================

    //firstMoveSelector(firstMove); 
                    

    // ============================= Main loop ================================
    uint32_t timout_time = millis();
    while (1) {
        handleDrive();
		// read all sensors
		lineSens = LineSensRead();
	    distSens = DistSensRead(); //Distance_sensor_value_Read();

		if( 0 < lineSens ){
			// line has detected
		    ActOnLineDetected();
			timout_time = millis();
		}
		else if( 0 < distSens ){
			// opponent spotted
			ActOnEnemySpotted();
			timout_time = millis();
		}
        else {
             //setDrivePwmNow(DRIVE_TURN_LEFT, TURN_PWM, TURN_PWM, 200);
        }

		if( 2000 < millis() - timout_time ){
			timout_time = millis();
			DoSomething();
            Servo_enable_Write(0);
		}
    }
}

//=============================================================================
// Private function definitions
//=============================================================================

//! \brief	Act on opponent spotted event
void ActOnEnemySpotted(void){


	    int distSens = DistSensRead(); //Distance_sensor_value_Read();


    	if( 0 < distSens ){
            int8_t avrg_angle_grad = CalcAvrgAngle();

        	int8_t pwm = 50;
            // Attack spotted opponent
        	if( 0 == avrg_angle_grad ){
                setDrivePwmNow(DRIVE_FORWARD, ATTACK_PWM, ATTACK_PWM, 50);
        	}else if( -60 >= avrg_angle_grad ){
        		// [-90; -60] grad
                setDrivePwmNow(DRIVE_TURN_LEFT , TURN_PWM, TURN_PWM, 50);
        	}else if( -30 >= avrg_angle_grad ){
        		// (-60; -30] grad
                setDrivePwmNow(DRIVE_FORWARD, pwm /3, pwm, 40);
        	}else if( 0 > avrg_angle_grad ){
        		// (-30; 0) grad
                setDrivePwmNow(DRIVE_FORWARD, ATTACK_PWM * 0.7, ATTACK_PWM, 50);
        	}else if( 60 <= avrg_angle_grad ){
        		// [60; 90] grad
                setDrivePwmNow(DRIVE_TURN_RIGHT , TURN_PWM, TURN_PWM, 50);            
        	}else if( 30 <= avrg_angle_grad ){
        		// [30; 60) grad
        		setDrivePwmNow(DRIVE_FORWARD, pwm, pwm / 3, 40);
        	}else if( 0 <= avrg_angle_grad ){
        		// (0; 30) grad
                setDrivePwmNow(DRIVE_FORWARD, ATTACK_PWM, ATTACK_PWM*0.7, 50);
        	}
    	}
        else {
            
            //CyDelay(50);
        }
       
}

//! \brief	Act on line detected event
void ActOnLineDetected(void){
	//int lineSens = LineSensRead();
     
	// Line_FL && Line_FR
	if(gLine.F_LEFT && gLine.F_RIGHT){
        setDrivePwmNow(DRIVE_BACKWARD, 40, 40, 40);
        setDrivePwmNext(DRIVE_TURN_LEFT, TURN_PWM, TURN_PWM, 200);
        
	}
	// Line_FL
	else if(gLine.F_LEFT){
        //setDrivePwmNow(DRIVE_BACKWARD, 40, 40, 150);
        setDrivePwmNext(DRIVE_TURN_RIGHT, TURN_PWM, TURN_PWM, 30);
	}
	// Line_FR
	else if(gLine.F_RIGHT){
        //setDrivePwmNow(DRIVE_BACKWARD, 40, 40, 150);
        setDrivePwmNext(DRIVE_TURN_LEFT, TURN_PWM, TURN_PWM, 30);
	}
    
}

//! \brief	Do something when opponent is not visible
void DoSomething(void){

    setDrivePwmNow(DRIVE_BACKWARD, SEARCH_PWM_L, SEARCH_PWM_R, 40);

}
/* [] END OF FILE */
