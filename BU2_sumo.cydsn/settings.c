/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "settings.h"
#include "project.h"
#include "stdio.h"
#include "stdlib.h"
#include "ssd1306.h"
#include "menu.h"
#include "sensors.h"
#include "drive.h"


const char settingsMenu_title[] = " #SET#";
// Menu Page1
const char settingsMenu_1[] = "1.5sec on";
const char settingsMenu_2[] = "2.basic speed";
const char settingsMenu_3[] = "3.turn speed";
// Menu Page2
const char settingsMenu_4[] = "4.EXIT";

const char* settingsMenu_strings[] = {settingsMenu_1, settingsMenu_2,settingsMenu_3,
	settingsMenu_4,};

void setline(void);
void setBasicSpeed(void);
void setTurnSpeed(void);
void fiveSeconDelay(void);
void flag(void);

void SettingsMenu(){
    Menu settingsMenu = {settingsMenu_title, settingsMenu_strings, 4, 0, 0};
    while( 1 ){
		// (Re)Initialize menu system and wait for menu item selection.
		int menuStatus = ShowMenu(&settingsMenu);

		switch( menuStatus ){
			case 0:  
				fiveSeconDelay();        
				break;
			case 1:  
				setBasicSpeed();
				break;
			case 2:  // 3.encoders
				
				break;
			case 3:  
			default:
				// Return to the main menu
				return;
		}
	}
}

void ReadEEPROMSettings(void){
    gLine.FTH_val = EEPROM_ReadByte(EEPROM_ADDR_LINE_DETECT);
    gMotor.basic_speed = EEPROM_ReadByte(EEPROM_ADDR_BASIC_SPEED);
    gMotor.turn_speed = EEPROM_ReadByte(EEPROM_ADDR_TURN_SPEED);
    gSettings.flag = EEPROM_ReadByte(EEPROM_ADDR_FLAG);
    gSettings.five_seconds = EEPROM_ReadByte(EEPROM_ADDR_FIVE_SECOND_DELAY);
    gSettings.lastTactic = EEPROM_ReadByte(EEPROM_ADDR_LAST_TACTIC);
}

void setline(void){
    char text[5];
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
    itoa(gLine.FTH_val, text, 10);
    display_clear(); 
    gfx_setCursor(30,0);
    gfx_print(text);  
    display_update();
            
    while (!GoBack()) {
        int UpDownState = ReadUpDownState();
        if (UpDownState > 0) {
            if (UpDownState == 1) {
                gLine.FTH_val--;    
            }
            else if (UpDownState == 2) {
                gLine.FTH_val++;    
            }
            int writeStatus = EEPROM_WriteByte(gLine.FTH_val,EEPROM_ADDR_LINE_DETECT);
            itoa(gLine.FTH_val, text, 10);
            display_clear(); 
            gfx_setCursor(30,0);
            gfx_print(text);  
            display_update();
        }
    }       
}

void setBasicSpeed(void){
    char text[5];
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
    itoa(gMotor.basic_speed, text, 10);
    display_clear(); 
    gfx_setCursor(30,0);
    gfx_print(text);  
    display_update();
            
    while (!GoBack()) {
        int UpDownState = ReadUpDownState();
        if (UpDownState > 0) {
            if (UpDownState == 1) {
                gMotor.basic_speed--;    
            }
            else if (UpDownState == 2) {
                gMotor.basic_speed++;    
            }
            int writeStatus = EEPROM_WriteByte(gMotor.basic_speed,EEPROM_ADDR_BASIC_SPEED);
            itoa(gMotor.basic_speed, text, 10);
            display_clear(); 
            gfx_setCursor(30,0);
            gfx_print(text);  
            display_update();
        }
    }       
}

void setTurnSpeed(void){
    char text[5];
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
    itoa(gMotor.turn_speed, text, 10);
    display_clear(); 
    gfx_setCursor(30,0);
    gfx_print(text);  
    display_update();
            
    while (!GoBack()) {
        int UpDownState = ReadUpDownState();
        if (UpDownState > 0) {
            if (UpDownState == 1) {
                gMotor.turn_speed--;    
            }
            else if (UpDownState == 2) {
                gMotor.turn_speed++;    
            }
            int writeStatus = EEPROM_WriteByte(gMotor.turn_speed,EEPROM_ADDR_TURN_SPEED);
            itoa(gMotor.basic_speed, text, 10);
            display_clear(); 
            gfx_setCursor(30,0);
            gfx_print(text);  
            display_update();
        }
    }       
}
void fiveSeconDelay (void){
    char text[5];
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
    itoa(gSettings.five_seconds, text, 10);
    display_clear(); 
    gfx_setCursor(30,0);
    gfx_print(text);  
    display_update();
            
    while (!GoBack()) {
        int UpDownState = ReadUpDownState();
        if (UpDownState > 0) {
            if (UpDownState == 1) {
                gSettings.five_seconds=1;    
            }
            else if (UpDownState == 2) {
                gSettings.five_seconds=0;    
            }
            int writeStatus = EEPROM_WriteByte(gSettings.five_seconds,EEPROM_ADDR_FIVE_SECOND_DELAY);
            itoa(gSettings.five_seconds, text, 10);
            display_clear(); 
            gfx_setCursor(30,0);
            gfx_print(text);  
            display_update();
        }
    }       
}


void flag (void){
    char text[5];
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
    itoa(gSettings.flag, text, 10);
    display_clear(); 
    gfx_setCursor(30,0);
    gfx_print(text);  
    display_update();
            
    while (!GoBack()) {
        int UpDownState = ReadUpDownState();
        if (UpDownState > 0) {
            if (UpDownState == 1) {
                gSettings.flag=1;    
            }
            else if (UpDownState == 2) {
                gSettings.flag=0;    
            }
            int writeStatus = EEPROM_WriteByte(gSettings.flag,EEPROM_ADDR_FLAG);
            itoa(gSettings.flag, text, 10);
            display_clear(); 
            gfx_setCursor(30,0);
            gfx_print(text);  
            display_update();
        }
    }       
}

/* [] END OF FILE */
