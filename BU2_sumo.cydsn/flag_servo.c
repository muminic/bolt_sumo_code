/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "flag_servo.h"

void flagServo_Init(){
    Servo_Start();
    
}

void servoAngle(int angle){
    if (angle >= 180) {angle = 180;}
    int pulseWidth = 300 + (angle * 5.6); // 1649  //0deg = 300, 180deg = 1300  period 20ms    
    Servo_WriteCompare(pulseWidth);
}

void FlagUp(void){
    servoAngle(90); 
}
void rightFlagDown(void){
    servoAngle(155);
}

void leftFlagDown(void){
    servoAngle(25);
}

//CyDelay(1000);
  //  servoAngle(20); 
  //  CyDelay(1000);
  //  servoAngle(90); 
  //  CyDelay(1000);
  //  servoAngle(160); 

//void flagDown(void){
    //servoAngle(90);    
//}
/* [] END OF FILE */
