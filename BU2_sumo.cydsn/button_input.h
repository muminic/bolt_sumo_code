/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef BUTTON_INPUT_H
#define BUTTON_INPUT_H

#include "stdio.h"

enum buttoncode_t {
	KEYCODE_NO_KEY = 0,
	KEYCODE_ENTER = 11,
	KEYCODE_UP = 12,
	KEYCODE_DOWN = 13,
};
typedef enum buttoncode_t buttoncode_t;

//! Keyboard key actions
enum buttonaction_t {
	KEYACTION_NO_ACTION,
	KEYACTION_PRESS,
	KEYACTION_RELEASE,
};
typedef enum buttonaction_t buttonaction_t;

//! Structure to hold a keycode and the related keyaction
struct keyboard_event_t {
	buttoncode_t buttoncode;
	buttonaction_t buttonaction;
};
typedef struct keyboard_event_t keyboard_event_t;


buttoncode_t IsKeyReleased(void);
int GoBack(void);
int ReadUpDownState();
void GetButtonState (keyboard_event_t *keyboardEvent);

#endif /* BUTTON-INPUT_H */
/* [] END OF FILE */
