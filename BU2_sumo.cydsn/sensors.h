/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef SENSORS_H
#define SENSORS_H

#include <project.h>
#include "timer_time.h"

//=============================================================================
// Public defines and typedefs
//=============================================================================
//! Masks for robot distance sensors ( L90 L45 L30 L00 R00 R30 R45 R90 )
enum distance_sensor {
	DISTSENS_L90 = 0b10000000,
	DISTSENS_L60 = 0b01000000,
	DISTSENS_L30 = 0b00100000,
	DISTSENS_L00 = 0b00010000,
	DISTSENS_R00 = 0b00001000,
	DISTSENS_R30 = 0b00000100,
	DISTSENS_R60 = 0b00000010,
	DISTSENS_R90 = 0b00000001
};

//! Masks for robot line sensors ( FL BL BR FR )
enum line_sensor {
	LINESENS_FL1 = 0b00010000,
	LINESENS_FL2 = 0b00001000,
    LINESENS_B =   0b00000100,
	LINESENS_FR2 = 0b00000010,
	LINESENS_FR1 = 0b00000001
    
};

 struct gLinevars{
    int FL1_val;
    int FL2_val;
    int FR1_val;
    int FR2_val;
	
    int B_val;
	int FTH_val;
    
	int F_LEFT;	
	int F_RIGHT;	
	int REAR;
	
	int adc_buff[15];
	
}gLine;

uint8_t DistSensRead(void);
void ResetDistSensors(void);
int8_t CalcAvrgAngle();
void WaitForIRStart(void);
uint8_t LineSensRead(void);

#endif /* SENSORS_H */

