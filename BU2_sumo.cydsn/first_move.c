/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "first_move.h"
#include "sensors.h"
#include "timer_time.h"
#include "drive.h"
#include "stdlib.h"

//=============================================================================
// Private defines and typedefs
//=============================================================================
#define Rd_bits(value, mask)        ((value)&(mask))

#define SEARCH_PWM_L  25
#define SEARCH_PWM_R  25
#define TURN_PWM    40
#define ATTACK_PWM  70


void firstMoveTurnRight();
void firstMoveTurnLeft();
void firstMoveFastForward();
void firstMoveDoubleFastForward();
void firstMoveDriveToLine();

void firstMoveSelector(FirstMove_t firstMove)
{
    switch(firstMove)
    {
        case DEFAULT:
            drive_forward(SEARCH_PWM_L, SEARCH_PWM_R);
            CyDelay(50);
        break;
        case FAST_FORWARD:
            firstMoveFastForward();
        break;
        case TURN_RIGHT_45:
            firstMoveTurnRight();
        break;
        case TURN_LEFT_45:
            firstMoveTurnLeft();
        break;
        case RIGHT_LINE_ARC:
            firstMoveRightLineArc();
        break;
        case DOUBLE_FAST_FORWARD:
            firstMoveDoubleFastForward();
        break;
        case DRIVE_TO_LINE:
            firstMoveDriveToLine();
        break;
        default:
            drive_forward(SEARCH_PWM_L, SEARCH_PWM_R);
        break;
    }
}

void firstMoveRightLineArc()
{
    //driveLineRight(1300);
    turn_left(50,70);
    CyDelay(250);
}

void firstMoveTurnRight()
{
    turn_right(40,40);
    CyDelay(50);    
}

void firstMoveTurnLeft()
{
    turn_left(40,40);
    CyDelay(50);    
}
void firstMoveFastForward()
{
    drive_forward(ATTACK_PWM, ATTACK_PWM);
    CyDelay(280);
    motors_brake();
    CyDelay(30);
    drive_forward(0,0);   
}

void firstMoveDoubleFastForward()
{
    drive_forward(ATTACK_PWM, ATTACK_PWM);
    CyDelay(450);
    motors_brake();
    CyDelay(30);
    drive_forward(ATTACK_PWM, ATTACK_PWM);
    CyDelay(40);
    motors_brake();
    CyDelay(30);
    drive_forward(0,0);   
}

void firstMoveDriveToLine()
{
    drive_forward(ATTACK_PWM, ATTACK_PWM);
    CyDelay(300);
    while(LineSensRead() == 0){
        drive_forward(40,40);
    }   
}



