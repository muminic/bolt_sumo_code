/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "drive_smart.h"
#include <project.h>
#include "drive.h"
#include "stdio.h"
#include <stdlib.h>
#include "ssd1306.h"
#include "timer_time.h"
#include "sensors.h"

#define LEFT	1
#define RIGHT	-1
#define STRAIGHT 0

// Robot gears - 10:23/18:26
// Gear ratio:                          1:3.322   ((23x26)/(10x18))
// Encoder ticks per motor rotation:    6 (5 magnets)
// Wheel diameter:                      3 cm

// practically 44 tics -> 20 cm (1 tic -> 0.455 cm)
#define DIST_TO_TICKS_SCALE_FACTOR      (2.115)     // - 1cm
// Precision: 1 tic -> 0.473 cm (3*pi/3.322/6)
#define ANGLE_TO_TICKS_SCALE_FACTOR     (0.258)     // - 1deg (2*7*pi/0.473/360)
// Precision: 1 tic -> 3.872 deg

//=============================================================================
// Public data definitions
//=============================================================================
uint32_t g_driveTime_ms = 0u;

//=============================================================================
// Private function declarations
//=============================================================================
static void Move_Distance(int8_t speed, uint16_t radius, int8_t side, uint16_t distance);
static void Move_Angle(int8_t speed, uint16_t radius, int8_t side, uint16_t angle);
static int8_t GetInnerWheelSpeed(int8_t speed_outer, uint16_t radius);
static uint16_t GetOutsideTicksCurve_Angle(uint16_t radius, uint16_t angle);
static uint16_t GetInsideTicksCurve_Angle(uint16_t radius, uint16_t angle);
static uint16_t GetTicksStraight(uint16_t distance);
static uint16_t GetOutsideTicksCurve(uint16_t radius, uint16_t distance);
static uint16_t GetInsideTicksCurve(uint16_t radius, uint16_t distance);


//=============================================================================
// Private data definitions
//=============================================================================
static uint32_t sIntervalCheck = 0u;
static driveState_t sDriveState = DRV_DONE;
static int sSpeedLeft_GOAL = 0;
static int sSpeedRight_GOAL = 0;
static int sSpeedLeft = 0;
static int sSpeedRight = 0;
static uint16_t sEncLeft_GOAL = 0;
static uint16_t sEncRight_GOAL = 0;
static uint16_t sEncLeft = 0;
static uint16_t sEncRight = 0;
static uint16_t sSide = STRAIGHT;
static uint16_t sRadius = 0;

static int16_t lastError = 0; 
static int16_t sumError = 0;
//static double sRatio = 0.0;


//=============================================================================
// Public function definitions
//=============================================================================
void drive_forward_s(int distance, int speed)
{
    //log_info("-drive_forward(%d, %d)", distance, speed);
    g_driveTime_ms = millis();
    sDriveState = DRV_SOFT_START;
    Move_Distance(speed, 0xFFFF, 0, distance);
}
void drive_backward_s(int distance, int speed)
{
    //log_info("-drive_backward(%d, %d)", distance, speed);
    g_driveTime_ms = millis();
    sDriveState = DRV_SOFT_START;
    Move_Distance(-speed, 0xFFFF, 0, distance);
}
void drive_turnLeft_s(int angle, int speed)
{
    //log_info("-drive_turnLeft(%d, %d)", angle, speed);
    angle -= (angle / 20);                  // NB: hack to deal with owershoot
    if (speed >= 70) angle -= (angle / 20); //
    sDriveState = DRV_SOFT_START;
    Move_Angle(speed, 0, LEFT, angle);
}
void drive_turnRight_s(int angle, int speed)
{
    //log_info("-drive_turnRight(%d, %d)", angle, speed);
    angle -= (angle / 20);                  // NB: hack to deal with owershoot
    if (speed >= 70) angle -= (angle / 20); //
    sDriveState = DRV_SOFT_START;
    Move_Angle(speed, 0, RIGHT, angle);
}
void drive_turnLeftImu(int angle, int speed)
{
    //vect3d euler; // Orientation (Euler angles) [deg]
    //imu_read_3d_vector(IMU_EULER, &euler);
    //int lastValue = euler.x * 0.0625;
    int8_t turnFinished = 0;
    int imu_angle = 0;
    int32_t motorSpeed = 0;
    int Kp = 20;
    int Kd = 50;//100
    int baseSpeed = speed;
    int lastError = 0; 
    int startTime = millis();
    
    while (turnFinished == 0)
    {
        //imu_read_3d_vector(IMU_EULER, &euler);
        //if (euler.x * 0.0625 >= 270) {
         //   imu_angle = -(360 - (euler.x * 0.0625));
        //} else {
        //    imu_angle = euler.x * 0.0625;
        //}
        int error = (angle + (imu_angle));
        motorSpeed = (Kp * error + Kd * (error - lastError)) / 10; // / 500 tev var nederēt
        if (motorSpeed >=0 ) {
            motors_drive(-motorSpeed, motorSpeed);
        } else {
            motors_drive(motorSpeed, -motorSpeed);    
        }
        lastError = error;
        display_clear();
        gfx_setCursor(0, 0);
        gfx_printNum((imu_angle)); 
        gfx_setCursor(0, 20);
        gfx_printNum((error));
        gfx_setCursor(0, 40);
        gfx_printNum((motorSpeed));
        if (abs(error) == 3 && abs(lastError) == 3 ) {
            //turnFinished = 1;
            motors_drive(0,0);
            motors_brake();
            return;
        }

        //display_update();

        //CyDelay(50);
        //if (lastError)
        if (millis() - startTime > 1000) {
            motors_brake();
            return;
        }
    }
}
void drive_turnRightImu_s(int angle, int speed)
{
    
}
void drive_arcLeft(int radius, int angle, int speed)
{
    sDriveState = DRV_SOFT_START;
    Move_Angle(speed, radius, LEFT, angle);
}
void drive_arcRight(int radius, int angle, int speed)
{
    sDriveState = DRV_SOFT_START;
    Move_Angle(speed, radius, RIGHT, angle);
}



void drive_arcLeftPID (int radius, int angle, int speed)
{
    int16_t left_speed = speed;
	int16_t right_speed = speed;

	uint16_t left_encoder = 0;
	uint16_t right_encoder = 0;

    int16_t EncLeft = 0;
    int16_t EncRight = 0;

	//right wheel is outside
	left_speed = GetInnerWheelSpeed(right_speed, radius);
	right_encoder = GetOutsideTicksCurve_Angle(radius, angle);
	left_encoder = GetInsideTicksCurve_Angle(radius, angle);

	ENC_L_Counter_WriteCounter(0);
    ENC_R_Counter_WriteCounter(0);

    //sSpeedLeft_GOAL = left_speed;
    //sSpeedRight_GOAL = right_speed;
    //sSpeedLeft = left_speed / 10;  // NB: sign important
    //sSpeedRight = right_speed / 10;
    
    int32_t speedCorrection = 0;
    int Kp =6;
    int Kd = 15;//100
    int lastError = 0; 
    int error = 0;
    
    long radiusFactor = (1000 - (14000 / (radius + 7)));
    drive_forward(left_speed/2, right_speed/2); //motors drive
    CyDelay(4);
    
    while (1) {
        EncLeft = ENC_L_Counter_ReadCounter();
        EncRight = ENC_R_Counter_ReadCounter();
        error = EncLeft  - (EncRight * radiusFactor/1000); //0.75 for driving with 55cm radius //5 - (10*0.75) = 5-7.5 = -2.5 
        
        if ((EncLeft + EncRight) >= 120) {
            Kp = 20;
            Kd = 50;
        }
        if ((EncLeft + EncRight) >= 35) {
            Kp = 12;
            Kd = 25;
        }
        
        
        speedCorrection = (Kp * error + Kd * (error - lastError))/2 ; 
        lastError = error;
        
        if (speed >= 0) {
            left_speed = (speed * radiusFactor/1000) - speedCorrection;
        } else {
            left_speed = (speed * radiusFactor/1000) + speedCorrection;    
        }
        right_speed = speed ;
              
        if (right_speed > 100) right_speed = 100;
        else if (right_speed < -100) right_speed = -100;
        
        if (left_speed > 100) left_speed = 100;
        else if (left_speed < -100) left_speed = -100;          

        //log_debug(" -drive Left arc (%d,%d,%d, %d ,%d)", EncLeft, EncRight, error, left_speed, right_speed);
        
        CyDelay(1);
        
        drive_forward(left_speed, right_speed); //drive_motors
        
        if (EncRight >= right_encoder || EncLeft >= left_encoder ) {
        motors_brake();
        //log_debug(" -drive arc finished (%d,%d,%d, %d ,%d)", EncLeft, EncRight, error, left_speed, right_speed);
        return;
                     
        }
        
        
    }
    
    //motors_drive(sSpeedLeft, sSpeedRight);    
}

void drive_arcRightPID(int radius, int angle, int speed)
{
    int16_t left_speed = speed;
	int16_t right_speed = speed;

	uint16_t left_encoder = 0;
	uint16_t right_encoder = 0;

    int16_t EncLeft = 0;
    int16_t EncRight = 0;

	//right wheel is outside
	right_speed = GetInnerWheelSpeed(left_speed, radius);
	right_encoder = GetInsideTicksCurve_Angle(radius, angle);
	left_encoder = GetOutsideTicksCurve_Angle(radius, angle);

	ENC_L_Counter_WriteCounter(0);
    ENC_R_Counter_WriteCounter(0);

    //sSpeedLeft_GOAL = left_speed;
    //sSpeedRight_GOAL = right_speed;
    //sSpeedLeft = left_speed / 10;  // NB: sign important
    //sSpeedRight = right_speed / 10;
    
    int32_t speedCorrection = 0;
    int Kp =5;
    int Kd = 15;//100
    int Ki = 1;
    int lastError = 0; 
    int error = 0;
    int16_t sumError = 0;
    
    long radiusFactor = (1000 - (14000 / (radius + 7)));
    motors_drive(left_speed/2, right_speed/2); 
    CyDelay(4);
    
    
    while (1) {
        EncLeft = ENC_L_Counter_ReadCounter();
        EncRight = ENC_R_Counter_ReadCounter();
        error = EncRight  - (EncLeft * radiusFactor/1000); //0.75 for driving with 55cm radius //5 - (10*0.75) = 5-7.5 = -2.5 
        if ((EncLeft + EncRight) >= 500) {
            Kp = 20;
            Kd = 50;
        }
        if ((EncLeft + EncRight) >= 100) {
            Kp = 10;
            Kd = 23;
        }
        sumError = sumError + error ; 
        speedCorrection = (Kp * error + Kd * (error - lastError) + (Ki * (sumError + error))/5)/2 ; 
        lastError = error;
        
        if (speed >=0) {
            right_speed = (speed * radiusFactor/1000) - speedCorrection;
        } else {
            right_speed = (speed * radiusFactor/1000) + speedCorrection;   
        }
  
        if (right_speed > 100) right_speed = 100;
        else if (right_speed < -100) right_speed = -100;
        left_speed = speed ; // + speedCorrection;
        if (left_speed > 100) left_speed = 100;
        else if (left_speed < -100) left_speed = -100;          

        //log_debug(" -drive Right arc (%d,%d,%d, %d ,%d)", EncLeft, EncRight, error, left_speed, right_speed);
        
        CyDelay(1);
        
        motors_drive(left_speed, right_speed); 
        
        if (EncRight >= right_encoder || EncLeft >= left_encoder ) {
        motors_brake();
        //log_debug(" -drive arc finished (%d,%d,%d, %d ,%d)", EncLeft, EncRight, error, left_speed, right_speed);
        return;
                    
        }
    }
}

void drive_breakTillStopped(void)
{
    // todo
}

void drive_backwardTillLineSpotted (int speed)
{
    
    while (1)
    {
        if ( LineSensRead() == 0 ) {
            //drive_backward(speed,speed);
            motors_drive(-speed, - speed);
            CyDelay(1);
        } else {
            motors_drive(speed, speed);
            CyDelay(3);
            drive_backward(0,0);
            motors_brake();
            CyDelay(10);
            return;
        }
    }
}

driveState_t drive_handleDrive(void)
{
    if (1 > millis() - sIntervalCheck) return sDriveState;
    sIntervalCheck = millis();

    sEncLeft = ENC_L_Counter_ReadCounter();
    sEncRight = ENC_R_Counter_ReadCounter();

    switch (sDriveState) {
    case DRV_SOFT_START:
        if (sSpeedLeft_GOAL >= 0) {
            // positive direction
            if (sSpeedLeft < sSpeedLeft_GOAL) {
                sSpeedLeft += 1;
            }
        } else {
            // negative direction
            if (sSpeedLeft > sSpeedLeft_GOAL) {
                sSpeedLeft -= 1;
            }
        }
        if (sSpeedRight_GOAL >= 0) {
            if (sSpeedRight < sSpeedRight_GOAL) {
                sSpeedRight += 1;
            }
        } else {
            if (sSpeedRight > sSpeedRight_GOAL) {
                sSpeedRight -= 1;
            }
        }
        motors_drive(sSpeedLeft, sSpeedRight);
        if ((sEncLeft_GOAL <= sEncLeft) && (sEncRight_GOAL <= sEncRight)) {
            // for short distances we don't reach GOAL speed
            sDriveState = DRV_DRIVE_PID;
        }
        if ((abs(sSpeedLeft) >= abs(sSpeedLeft_GOAL)) && (abs(sSpeedRight) >= abs(sSpeedRight_GOAL))) {
            //log_debug(" DRV_SOFT_START(%d, %d) enc", sEncLeft, sEncRight);
            sDriveState = DRV_DRIVE_PID;
        }
        break;
    case DRV_FAST_SOFT_START:
        if (sSpeedLeft_GOAL >= 0) {
            // positive direction
            if (sSpeedLeft < sSpeedLeft_GOAL) {
                sSpeedLeft += 2;
            }
        } else {
            // negative direction
            if (sSpeedLeft > sSpeedLeft_GOAL) {
                sSpeedLeft -= 2;
            }
        }
        if (sSpeedRight_GOAL >= 0) {
            if (sSpeedRight < sSpeedRight_GOAL) {
                sSpeedRight += 2;
            }
        } else {
            if (sSpeedRight > sSpeedRight_GOAL) {
                sSpeedRight -= 2;
            }
        }
        motors_drive(sSpeedLeft, sSpeedRight);
        if ((sEncLeft_GOAL <= sEncLeft) && (sEncRight_GOAL <= sEncRight)) {
            // for short distances we don't reach GOAL speed
            sDriveState = DRV_DRIVE_PID;
        }
        if ((abs(sSpeedLeft) >= abs(sSpeedLeft_GOAL)) && (abs(sSpeedRight) >= abs(sSpeedRight_GOAL))) {
            log_debug(" DRV_SOFT_START(%d, %d) enc", sEncLeft, sEncRight);
            sDriveState = DRV_DRIVE_PID;
        }
        break;
    case DRV_DRIVE:
        // todo: implement soft brake
        if ((sEncLeft_GOAL <= sEncLeft) && (sEncRight_GOAL <= sEncRight)) {
            sSpeedLeft = 0;
            sSpeedRight = 0;
            sDriveState = DRV_DONE;
            g_driveTime_ms = millis() - g_driveTime_ms;
            motors_brake();
            log_debug(" DRV_DONE");
        }
        break;
    case DRV_DRIVE_PID:
        log_debug(" DRV_DRIVE_PID (%d) (%d)", sSpeedLeft_GOAL, sSpeedRight_GOAL );
        int16_t left_speed = sSpeedLeft_GOAL; 
	    int16_t right_speed = sSpeedRight_GOAL;

        int32_t speedCorrection = 0;
        int Kp =6;
        int Kd = 15;
        int Ki = 1;
        int error = 0;
        long radiusFactor = 1;
        
        if ((sEncLeft + sEncRight) >= 500) {
            Kp = 20;
            Kd = 50;
        } else if ((sEncLeft + sEncRight) >= 100) {
            Kp = 12;
            Kd = 25;
        }
        
        if (sRadius > 0){
            if (sSide == LEFT ) {
                radiusFactor = 1000*left_speed/right_speed;
                error = sEncLeft  - (sEncRight * radiusFactor/1000); //PD regulator
            }else {  
                radiusFactor = 1000*right_speed/left_speed;
                error = sEncRight  - (sEncLeft * radiusFactor/1000); //PD regulator
            }
        } else {
         error = 0;    
        }
            
        sumError = sumError + error ; 
        speedCorrection = (Kp * error + Kd * (error - lastError)+ (Ki * (sumError + error))/5)/2 ; 
        lastError = error;  
        
        if (sSide == LEFT ) {
            if (left_speed >= 0) {
                left_speed = (left_speed) - speedCorrection;
            } else {
                left_speed = (left_speed) + speedCorrection;    
            }
        } else {
            if (right_speed >=0) {
                right_speed = (right_speed) - speedCorrection;
            } else {
                right_speed = (right_speed) + speedCorrection;   
            }    
        }
                  
        if (right_speed > 100) right_speed = 100;
        else if (right_speed < -100) right_speed = -100;
            
        if (left_speed > 100) left_speed = 100;
        else if (left_speed < -100) left_speed = -100;          
 
        motors_drive(left_speed, right_speed); 

        if ((sEncLeft_GOAL <= sEncLeft) && (sEncRight_GOAL <= sEncRight)) {
            sSpeedLeft = 0;
            sSpeedRight = 0;
            sDriveState = DRV_DONE;
            g_driveTime_ms = millis() - g_driveTime_ms;
            motors_brake();
            log_debug(" DRV_DONE");
        }
        break;
    case DRV_DONE:
        break;
    default:
        break;
    }

    return sDriveState;
}

// NB: blocking
void drive_waitFor_DONE(void)
{
    const uint32_t timoutTime = millis();
    while (1) {
        const driveState_t driveState = drive_handleDrive();
        if (DRV_DONE == driveState) break;
        CyDelayUs(10);

        if (3000 < millis() - timoutTime) {
            // should not take so long!
            break;
        }
    }
}
// NB: blocking
void drive_waitFor_LineSpotted(void)
{
    const uint32_t timoutTime = millis();
    while (1) {
        const driveState_t driveState = drive_handleDrive();
        if ( LineSensRead() == 0 ) {
            CyDelay(1);
        } else {
            motors_drive(30,30);
            CyDelay(5);
            motors_brake();
            CyDelay(5);
            sDriveState = DRV_DONE;
            return;
        }
        if (3000 < millis() - timoutTime) {
            // should not take so long!
            break;
        }
    }
}

//=============================================================================
// Private function definitions
//=============================================================================
// NB: negative speed means backwards
void Move_Distance(int8_t speed, uint16_t radius, int8_t side, uint16_t distance)
{
	int8_t left_speed = speed;
	int8_t right_speed = speed;
	uint16_t left_encoder = 0;
	uint16_t right_encoder = 0;

	if (radius < 0xFFFF) {
		if (side == LEFT) {
			//right wheel is outside
			left_speed = GetInnerWheelSpeed(right_speed, radius);
			right_encoder = GetOutsideTicksCurve(radius, distance);
			left_encoder = GetInsideTicksCurve(radius, distance);
            sSide = LEFT;
		}
		else {
			//left wheel is outside
			right_speed = GetInnerWheelSpeed(left_speed, radius);
			left_encoder = GetOutsideTicksCurve(radius, distance);
			right_encoder = GetInsideTicksCurve(radius, distance);
            sSide = RIGHT;
		}
	}
	else {
	    left_encoder = GetTicksStraight(distance);
		right_encoder = left_encoder;
        sSide = STRAIGHT;
        sRadius = radius;
	}
    sRadius = radius;

    //sRatio = left_encoder / right_encoder;
    sEncLeft_GOAL = left_encoder;
    sEncRight_GOAL = right_encoder;
	ENC_L_Counter_WriteCounter(0);
    ENC_R_Counter_WriteCounter(0);

    sSpeedLeft_GOAL = left_speed;
    sSpeedRight_GOAL = right_speed;
    sSpeedLeft = left_speed / 20;  // NB: sign important
    sSpeedRight = right_speed / 20;

    log_debug(" -Move_Distance(%d, %d) enc", sEncLeft_GOAL, sEncRight_GOAL);
    motors_drive(sSpeedLeft, sSpeedRight);
}

// NB: negative speed means backwards
// do nothing if R=INF
void Move_Angle(int8_t speed, uint16_t radius, int8_t side, uint16_t angle)
{
	int8_t left_speed = speed;
	int8_t right_speed = speed;

	uint16_t left_encoder = 0;
	uint16_t right_encoder = 0;

	if (radius < 0xFFFF) {		
		if (side == LEFT) {
			//right wheel is outside
			left_speed = GetInnerWheelSpeed(right_speed, radius);
			right_encoder = GetOutsideTicksCurve_Angle(radius, angle);
			left_encoder = GetInsideTicksCurve_Angle(radius, angle);
            sSide = LEFT;
		}
		else {
			//left wheel is outside
			right_speed = GetInnerWheelSpeed(left_speed, radius);
			left_encoder = GetOutsideTicksCurve_Angle(radius, angle);
			right_encoder = GetInsideTicksCurve_Angle(radius, angle);
            sSide = RIGHT;
		}
	}
	else {
		left_speed = 0;
		right_speed = 0;
		left_encoder = 0;
		right_encoder = 0;
	}
    sRadius = radius;

    sEncLeft_GOAL = left_encoder;
    sEncRight_GOAL = right_encoder;
	ENC_L_COUNTER_WriteCounter(0);
    ENC_R_COUNTER_WriteCounter(0);

    sSpeedLeft_GOAL = left_speed;
    sSpeedRight_GOAL = right_speed;
    log_debug(" Move Angle Speed Goal (%d) (%d)", sSpeedLeft_GOAL,sSpeedRight_GOAL);
    sSpeedLeft = left_speed / 10;  // NB: sign important
    sSpeedRight = right_speed / 10;
    
    log_debug(" Move Angle (%d) (%d)", sSpeedLeft,sSpeedRight);
    motors_drive(sSpeedLeft, sSpeedRight);
    
}

// returns inner wheel speed
// wheel center offset: 4cm
// Ticks per rotation: 14       6 8
// Gear ratio: 3.322
// wheel diameter: 3cm  
// Tick per cm: 4.9             2.115
// Ticks per degree: 0.6        0.258
int8_t GetInnerWheelSpeed(int8_t speed_outer, uint16_t radius_cm)
{
	const int16_t Cs = 1000 - (14000 / (radius_cm + 4));
	return ((int16_t)speed_outer * Cs) / 1000;
}
// For driving set Angle on curve or on place if R=0
uint16_t GetOutsideTicksCurve_Angle(uint16_t radius_cm, uint16_t angle_deg)
{
	return ((radius_cm + 4) * angle_deg * 10) / 609;
}
// For driving set Angle on curve or on place if R=0
uint16_t GetInsideTicksCurve_Angle(uint16_t radius_cm, uint16_t angle_deg)
{
	int16_t tmp = (((int16_t)radius_cm - 4) * angle_deg * 10) / 609;
	if (tmp < 0) tmp *= -1;
	return (uint16_t)tmp;
}
// For driving set distance straight
uint16_t GetTicksStraight(uint16_t distance_cm)
{
	return (distance_cm * 94) / 100;
}
// For driving set Distance on curve or on place if R=0
uint16_t GetOutsideTicksCurve(uint16_t radius_cm, uint16_t distance_cm)
{
	if (radius_cm == 0) {
		return ((distance_cm * 94) / 100);
	}

	return ((((radius_cm + 4) * distance_cm)) / radius_cm);
}
// For driving set Distance on curve or on place if R=0
uint16_t GetInsideTicksCurve(uint16_t radius_cm, uint16_t distance_cm)
{
    CYASSERT(radius_cm == 0 || radius_cm > 4)

	if (radius_cm == 0) {
		return (distance_cm * 94) / 100;
	}

	return ((((radius_cm - 4) * distance_cm)) / radius_cm);
}




/* [] END OF FILE */
