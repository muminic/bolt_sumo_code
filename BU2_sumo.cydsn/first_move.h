/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef FIRST_MOVE_H
#define FIRST_MOVE_H

#include "stdlib.h"
    
//=============================================================================
// Public function declarations
//=============================================================================
typedef enum{ 
    DEFAULT,
    FAST_FORWARD,
    TURN_LEFT_45,
    TURN_RIGHT_45,
    RIGHT_LINE_ARC,
    DOUBLE_FAST_FORWARD,
    DRIVE_TO_LINE
}FirstMove_t;
    
    
    
void firstMoveRightLineArc();
void firstMoveSelector(FirstMove_t firstMove);
#endif /* TACTIC1_H */
/* [] END OF FILE */
