/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "drive.h"
#include "project.h"
#include "sensors.h"
#include "timer_time.h"
#include "stdlib.h"

#define TICKS_PER_REVOLUTION 5
#define MAX_PWM     95
#define MIN_PWM     5
#define CM_TO_TICKS_SCALE  2 //1.5  // need to calibrate
#define ANGLE_TO_TICKS_SCALE 0.06 // need to calibrate

// practically 44 tics -> 20 cm (1 tic -> 0.455 cm)
#define DIST_TO_TICKS_SCALE_FACTOR      (0.94)     // - 1cm
// Precision: 1 tic -> 1.059 cm (2.7*pi/1/8)
#define ANGLE_TO_TICKS_SCALE_FACTOR     (0.258)     // - 1deg (2*7*pi/0.473/360)
// Precision: 1 tic -> 3.872 deg





void Motor_Init(void){
    MOT_PWM_Start();
    MOT_CTRL_reg_Write(0); 
	MOT_CTRL_reg_1_Write(0);
}

void motors_drive(int left_speed, int right_speed)
{
if (left_speed >=0 && right_speed >=0)
    {drive_forward(left_speed, right_speed);}
else if (left_speed >=0)
{
    {turn_right(left_speed, right_speed);}
}
else if (right_speed >=0)
{
    {turn_left(left_speed, right_speed);}
}
else {
    drive_backward(left_speed, right_speed);
}
}


void drive_backward(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x8); 
	MOT_CTRL_reg_1_Write(0x2);
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
}

void soft_drive_forward(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x4); 
	MOT_CTRL_reg_1_Write(0x1);
    while(right_pwm != gMotor.max_pwm_Limited_R && left_pwm != gMotor.max_pwm_Limited_L ) {
		// Soft start implementation
		if( left_pwm > gMotor.max_pwm_Limited_L ){
			gMotor.max_pwm_Limited_L += 5;
			if( left_pwm < gMotor.max_pwm_Limited_L ){
				gMotor.max_pwm_Limited_L = left_pwm;
			}
		}
		if( right_pwm > gMotor.max_pwm_Limited_R ){
			gMotor.max_pwm_Limited_R += 5;
			if( right_pwm < gMotor.max_pwm_Limited_R ){
				gMotor.max_pwm_Limited_R = right_pwm;
			}
		}        
        CyDelay(50);
        MOT_PWM_WriteCompare1(gMotor.max_pwm_Limited_L);
        MOT_PWM_WriteCompare2(gMotor.max_pwm_Limited_R);   
    }
    MOT_PWM_WriteCompare1(gMotor.max_pwm_Limited_L);
    MOT_PWM_WriteCompare2(gMotor.max_pwm_Limited_R);
    
}

void drive_forward(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x4); 
	MOT_CTRL_reg_1_Write(0x1);
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    
}

void turn_left(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x8); 
	MOT_CTRL_reg_1_Write(0x1);
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
}

void turn_right(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x4); 
	MOT_CTRL_reg_1_Write(0x2);
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
}

void motors_brake(void){
    MOT_CTRL_reg_Write(0xF); 
	MOT_CTRL_reg_1_Write(0xF);
    MOT_PWM_WriteCompare1(0);
    MOT_PWM_WriteCompare2(0);
    gMotor.max_pwm_Limited_L = 5;
    gMotor.max_pwm_Limited_R = 5;
    CyDelay(10);
}

void turn_simetrical_right(int speed)
{
    int enc_right=ENC_R_Counter_ReadCounter();
    int enc_left=ENC_L_Counter_ReadCounter();
    int side_difference = - enc_left + enc_right;
    int side_correction  = side_difference * 5;
    int speedL = speed + side_correction;
    int speedR = speed - side_correction; 
                        
                        
    if(speedL < MIN_PWM){speedL = MIN_PWM;} 
   	else if(speedL > MAX_PWM){speedL = MAX_PWM;} 
    if(speedR < MIN_PWM){speedR = MIN_PWM;} 
    else if(speedR > MAX_PWM){speedR = MAX_PWM;}    
                        
    //drive_motors(speedL, -speedR);
    turn_right(speedL, speedR);
}
void turn_simetrical_left(int speed)
{
    int enc_right=ENC_R_Counter_ReadCounter();
    int enc_left=ENC_L_Counter_ReadCounter();
    int side_difference = - enc_left + enc_right;
    int side_correction  = side_difference * 5;
    int speedL = speed + side_correction;
    int speedR = speed - side_correction; 
                        
                        
    if(speedL < MIN_PWM){speedL = MIN_PWM;} 
   	else if(speedL > MAX_PWM){speedL = MAX_PWM;} 
    if(speedR < MIN_PWM){speedR = MIN_PWM;} 
    else if(speedR > MAX_PWM){speedR = MAX_PWM;}    
                        
    //drive_motors(speedL, -speedR);
    turn_left(speedL, speedR);
}


void drive_distance_straight_forward(int distance, int speed)
{
    int dist = 0;
    ENC_R_Counter_WriteCounter(0);
    ENC_L_Counter_WriteCounter(0);
        
    while(dist < distance)
    {
        int enc_right = ENC_R_Counter_ReadCounter();
        int enc_left = ENC_L_Counter_ReadCounter();
        dist = ((enc_right + enc_left)/2) * CM_TO_TICKS_SCALE ; 
        int side_difference = enc_right - enc_left;
        int side_correction  = side_difference * 6;
        int speedL = speed + side_correction;
        int speedR = (speed) - side_correction;
                                    
        if(speedL < MIN_PWM){speedL = MIN_PWM;} 
       	else if(speedL > MAX_PWM){speedL = MAX_PWM;} 
        if(speedR < MIN_PWM){speedR = MIN_PWM;} 
        else if(speedR > MAX_PWM){speedR = MAX_PWM;}    
                            
        drive_forward(speedL, speedR);
    }
    
}
void turn_angle_right(int angle, int speed)
{
    int angle_done = 0;
    ENC_R_Counter_WriteCounter(0);
    ENC_L_Counter_WriteCounter(0);
    int ticks = angle * ANGLE_TO_TICKS_SCALE ; 
    while (!angle_done)
    {
        int enc_right = ENC_R_Counter_ReadCounter();
        int enc_left = ENC_L_Counter_ReadCounter();
        int enc_average= (enc_right + enc_left)/2 ;
        if (enc_average >= ticks)
        {
            angle_done = 1;
            
            ENC_R_Counter_WriteCounter(0);
            ENC_L_Counter_WriteCounter(0);
        }
        else
        {
            turn_simetrical_right(speed);
        }
    }
    drive_forward(0,0);
}
void turn_angle_left(int angle, int speed)
{
    int angle_done = 0;
    ENC_R_Counter_WriteCounter(0);
    ENC_L_Counter_WriteCounter(0);
    int ticks = angle * ANGLE_TO_TICKS_SCALE ; 
    while (!angle_done)
    {
        int enc_right = ENC_R_Counter_ReadCounter();
        int enc_left = ENC_L_Counter_ReadCounter();
        int enc_average= (enc_right + enc_left)/2 ;
        if (enc_average >= ticks)
        {
            angle_done = 1;
            
            ENC_R_Counter_WriteCounter(0);
            ENC_L_Counter_WriteCounter(0);
        }
        else
        {
            turn_simetrical_left(speed);
        }
    }
    drive_forward(0,0);
}

void drive_right_arc_forward(int distance, int speed)
{
    int dist = 0;
    ENC_R_Counter_WriteCounter(0);
    ENC_L_Counter_WriteCounter(1);
    
    while(dist < distance)
    {     
        int enc_right = ENC_R_Counter_ReadCounter();
        int enc_left = ENC_L_Counter_ReadCounter();
        dist = enc_right * CM_TO_TICKS_SCALE ;
        int side_difference = (enc_right* 0.58) - enc_left ; //0.77 for driving with 70cm radius
        int side_correction  = side_difference * 6;
        unsigned int speedL = speed + side_correction;
        unsigned int speedR = (speed) - side_correction;
                            
                            
        if(speedL < MIN_PWM){speedL = MIN_PWM;} 
       	else if(speedL > MAX_PWM){speedL = MAX_PWM;} 
        if(speedR < MIN_PWM){speedR = MIN_PWM;} 
        else if(speedR > MAX_PWM){speedR = MAX_PWM;}    
                            
        drive_forward(speedL, speedR);
    }
}



/* [] END OF FILE */
