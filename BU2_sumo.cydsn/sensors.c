/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "sensors.h"
#include "menu.h"
#include "settings.h"

#define Rd_bits(value, mask)        ((value)&(mask))

#define LINE_DETECT 250


void WaitForIRStart(void) {
    // Safety measure to check if start is already pressed!
    if (gSettings.lastState == 0) {
        if (1u == IR_START_Read()) {
            // Error - IR button is already pressed!!!
            while (1) {
                showText(25, "ERROR");
            }
        }

        while (0u == IR_START_Read()) {
            // wait for IR start forever
        }
        if (gSettings.five_seconds == 1)
        {
            CyDelay(4950);
        }
                
        //gSettings.lastState = 1;
        int writeStatus = EEPROM_WriteByte(gSettings.lastTactic,EEPROM_ADDR_LAST_TACTIC);
        }
    
}

uint8_t DistSensRead(void) {
	uint8_t value = 0b00000000;

	if( 0u == IRL_L90_Read() ){
        value |= DISTSENS_L90;
	}
	if( 0u == IRL_L60_Read() ){
		value |= DISTSENS_L60;
	}
	if( 0u == IRL_L30_Read() ){
		value |= DISTSENS_L30;
	}
	if( 0u == IRL_R0_Read() ){
		value |= DISTSENS_R00;
	}
	if( 0u == IRL_R30_Read() ){
		value |= DISTSENS_R30;
	}
	if( 0u == IRL_R60_Read() ){
		value |= DISTSENS_R60;
	}
	if( 0u == IRL_R90_Read() ){
		value |= DISTSENS_R90;
	}
	return value;
}

void ResetDistSensors(void) {
    Sensor_Reset_Write(0xFF);  
    CyDelay(1);
    Sensor_Reset_Write(0);
}

int8_t CalcAvrgAngle(){ 
    uint8_t distSens = DistSensRead(); //Distance_sensor_value_Read(); //
    //Sensor_Reset_Write(0xFF);
	int16_t angle = 0;
	uint8_t see_sensor_cnt = 0;

	if( Rd_bits(distSens, DISTSENS_L00) ){		// L01
		angle = angle - 5;
		see_sensor_cnt++;
	}
	if( Rd_bits(distSens, DISTSENS_L30) ){		// L30
		angle = angle - 30;
		see_sensor_cnt++;
	}
	if( Rd_bits(distSens, DISTSENS_L60) ){		// L45
        if( 0u == IRL_L60_Read() ){
    		angle = angle - 60;
    		see_sensor_cnt++;
        }
	}
	if( Rd_bits(distSens, DISTSENS_L90) ){		// L90
		angle = angle - 90;
		see_sensor_cnt++;
	}
	if( Rd_bits(distSens, DISTSENS_R90) ){		// R90
		angle = angle + 90;
		see_sensor_cnt++;
	}
	if( Rd_bits(distSens, DISTSENS_R60) ){		// R45
		angle = angle + 60;
		see_sensor_cnt++;
	}
	if( Rd_bits(distSens, DISTSENS_R30) ){		// R30
		angle = angle + 30;
		see_sensor_cnt++;
	}
	if( Rd_bits(distSens, DISTSENS_R00) ){		// R01
		angle = angle + 5;
		see_sensor_cnt++;
	}
    //Sensor_Reset_Write(0);
	return (int8_t)(angle / see_sensor_cnt);
}

uint8_t LineSensRead(void) {
	uint8_t value = 0b00000000;
    gLine.FTH_val = 250;
    if (ADC_SAR_IsEndConversion(ADC_SAR_RETURN_STATUS) > 0)
    {
       
        gLine.FL1_val = ADC_SAR_GetResult16(0);
        gLine.FL2_val = ADC_SAR_GetResult16(1);
      
        gLine.FR1_val = ADC_SAR_GetResult16(4);
        gLine.FR2_val = ADC_SAR_GetResult16(3);
    
         //int16 lineBack = ADC_SAR_GetResult16(6);
        
        //int16 lineUP1 = ADC_SAR_GetResult16(2);
        //int16 lineUP2 = ADC_SAR_GetResult16(5);

        //if (lineFL1 < LINE_DETECT) {
            //value |= LINESENS_FL1;
        //}
        //if (lineFL2 < LINE_DETECT) {
            //value |= LINESENS_FL2;
        //}
        //if (lineFR1 < LINE_DETECT) {
            //value |= LINESENS_FR1;
        //}
        //if (lineFR2 < LINE_DETECT) {
            //value |= LINESENS_FR2;
        //}
        //if (lineBack < LINE_DETECT) {
        //    //value |= LINESENS_B;
        
        
    gLine.F_LEFT = 0;
	if(gLine.FL1_val<gLine.FTH_val){gLine.F_LEFT|=0x01; value = 1;}
	if(gLine.FL2_val<gLine.FTH_val){gLine.F_LEFT|=0x02; value = 1;}
    
    gLine.F_RIGHT = 0;
	if(gLine.FR1_val<gLine.FTH_val){gLine.F_RIGHT|=0x01; value = 1;}
	if(gLine.FR2_val<gLine.FTH_val){gLine.F_RIGHT|=0x02; value = 1;}
        
    
    //gLine.REAR = 0;
	
   //     if(gLine.RL_val<gLine.RTH_val){gLine.REAR|=0x01;}
   // }
    }
	return value;
}



/* [] END OF FILE */
