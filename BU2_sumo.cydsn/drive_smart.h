/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef DRIVE_SMART_H
#define DRIVE_SMART_H

#include <stdint.h>

//=============================================================================
// Public defines and typedefs
//=============================================================================
typedef enum {
    DRV_NONE = 0,
	DRV_SOFT_START,
    DRV_FAST_SOFT_START,
    DRV_DRIVE,
    DRV_DRIVE_PID,
	DRV_DONE
} driveState_t;



//=============================================================================
// Public data declarations
//=============================================================================
extern uint32_t g_driveTime_ms;

//=============================================================================
// Public function declarations
//=============================================================================
void drive_forward(int distance, int speed);
void drive_backward(int distance, int speed);
void drive_turnLeft(int angle, int speed);
void drive_turnRight(int angle, int speed);
void drive_turnLeftImu(int angle, int speed); //not working
void drive_turnRightImu(int angle, int speed); //not working

void drive_arcLeft(int radius, int angle, int speed);
void drive_arcRight(int radius, int angle, int speed);
void drive_arcLeftPID (int radius, int angle, int speed);
void drive_arcRightPID(int radius, int angle, int speed);

void drive_backwardTillLineSpotted (int speed);

// drive arc forward/backward
void drive_breakTillStopped(void);
driveState_t drive_handleDrive(void);
void drive_waitFor_DONE(void);
void drive_waitFor_LineSpotted(void);

#endif /* DRIVE_H */
/* [] END OF FILE */
