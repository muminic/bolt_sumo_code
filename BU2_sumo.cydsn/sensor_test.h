/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdio.h>

    int16_t ax, ay, az, i;
	int16_t gx, gy, gz;


void SensorMenu();
void opponentSensors(void);
void lineSensors(void);
void encoderSensors(void);
void imuSensors(void);
void test(void);

/* [] END OF FILE */
